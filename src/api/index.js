import { Router } from 'express'
import user from './user'
import auth from './auth'
import passwordReset from './password-reset'
import blog from './blog'
import shop from './shop'
import items from './items'
import invoice from './invoice'
import category from './category'
import tax from './tax'
import contacts from './contacts'
import accountType from './account-type'
import accounts from './accounts'
import transaction from './transaction'
import logs from './logs'
import stock from './stock'

const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/password-resets', passwordReset)
router.use('/blogs', blog)
router.use('/shops', shop)
router.use('/items', items)
router.use('/invoices', invoice)
router.use('/categories', category)
router.use('/taxes', tax)
router.use('/contacts', contacts)
router.use('/accounttype', accountType)
router.use('/accounts', accounts)
router.use('/transactions', transaction)
router.use('/logs', logs)
router.use('/stocks', stock)

export default router
