import mongoose, { Schema } from 'mongoose'
// inveentory quantity type -  initial , adjust, sales, purchase
const qtyType = ['i', 'a', 's', 'p' ];

const stockSchema = new Schema({
  product_id: {
    type: String
  },
  quantity: {
    type: Number
  },
  quantityRate: {
    type: Number
  },
  source: {
    type: String,
    enum: qtyType,
    default: 'i'
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop',
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

stockSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      product_id: this.product_id,
      quantity: this.quantity,
      source: this.source,
      shop: this.shop,
      quantityRate: this.quantityRate,
      createdBy: this.createdBy,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Stock', stockSchema)

export const schema = model.schema
export default model
