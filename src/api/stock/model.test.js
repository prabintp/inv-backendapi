import { Stock } from '.'

let stock

beforeEach(async () => {
  stock = await Stock.create({ product_id: 'test', quantity: 'test', source: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = stock.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(stock.id)
    expect(view.product_id).toBe(stock.product_id)
    expect(view.quantity).toBe(stock.quantity)
    expect(view.source).toBe(stock.source)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = stock.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(stock.id)
    expect(view.product_id).toBe(stock.product_id)
    expect(view.quantity).toBe(stock.quantity)
    expect(view.source).toBe(stock.source)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
