import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Stock } from '.'

const app = () => express(apiRoot, routes)

let stock

beforeEach(async () => {
  stock = await Stock.create({})
})

test('POST /stocks 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ product_id: 'test', quantity: 'test', source: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.product_id).toEqual('test')
  expect(body.quantity).toEqual('test')
  expect(body.source).toEqual('test')
})

test('GET /stocks 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /stocks/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${stock.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(stock.id)
})

test('GET /stocks/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /stocks/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${stock.id}`)
    .send({ product_id: 'test', quantity: 'test', source: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(stock.id)
  expect(body.product_id).toEqual('test')
  expect(body.quantity).toEqual('test')
  expect(body.source).toEqual('test')
})

test('PUT /stocks/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ product_id: 'test', quantity: 'test', source: 'test' })
  expect(status).toBe(404)
})

test('DELETE /stocks/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${stock.id}`)
  expect(status).toBe(204)
})

test('DELETE /stocks/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
