import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { master, token } from '../../services/passport'
import { schema } from './model'
export Stock, { schema } from './model'

const router = new Router()
const { product_id, quantity, source, shop, quantityRate } = schema.tree

/**
 * @api {post} /stocks Create stock
 * @apiName CreateStock
 * @apiGroup Stock
 * @apiParam product_id Stock's product_id.
 * @apiParam quantity Stock's quantity.
 * @apiParam source Stock's source.
 * @apiSuccess {Object} stock Stock's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stock not found.
 */
router.post('/',
  
  body({ product_id, quantity, source, shop, quantityRate }),
  create)

/**
 * @api {get} /stocks Retrieve stocks
 * @apiName RetrieveStocks
 * @apiGroup Stock
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of stocks.
 * @apiSuccess {Object[]} rows List of stocks.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /stocks/:id Retrieve stock
 * @apiName RetrieveStock
 * @apiGroup Stock
 * @apiSuccess {Object} stock Stock's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stock not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /stocks/:id Update stock
 * @apiName UpdateStock
 * @apiGroup Stock
 * @apiParam product_id Stock's product_id.
 * @apiParam quantity Stock's quantity.
 * @apiParam source Stock's source.
 * @apiSuccess {Object} stock Stock's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stock not found.
 */
router.put('/:id',
  token({ required: true }),
  body({ product_id, quantity, source, shop, quantityRate }),
  update)

/**
 * @api {delete} /stocks/:id Delete stock
 * @apiName DeleteStock
 * @apiGroup Stock
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Stock not found.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
