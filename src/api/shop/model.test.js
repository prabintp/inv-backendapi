import { Shop } from '.'

let shop

beforeEach(async () => {
  shop = await Shop.create({ name: 'test', address[place: 'test', pin]: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = shop.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(shop.id)
    expect(view.name).toBe(shop.name)
    expect(view.address[place).toBe(shop.address[place)
    expect(view.pin]).toBe(shop.pin])
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = shop.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(shop.id)
    expect(view.name).toBe(shop.name)
    expect(view.address[place).toBe(shop.address[place)
    expect(view.pin]).toBe(shop.pin])
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
