import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Shop } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, shop

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  shop = await Shop.create({})
})

test('POST /shops 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, name: 'test', address : 'test', place : 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.address).toEqual('test')
  expect(body.place).toEqual('test')
})

test('POST /shops 401 (admin)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('POST /shops 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /shops 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /shops 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /shops 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /shops/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${shop.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(shop.id)
})

test('GET /shops/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${shop.id}`)
  expect(status).toBe(401)
})

test('GET /shops/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /shops/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${shop.id}`)
    .send({ access_token: masterKey, name: 'test', address[place: 'test', pin]: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(shop.id)
  expect(body.name).toEqual('test')
  expect(body.address[place).toEqual('test')
  expect(body.pin]).toEqual('test')
})

test('PUT /shops/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${shop.id}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('PUT /shops/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${shop.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /shops/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${shop.id}`)
  expect(status).toBe(401)
})

test('PUT /shops/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, name: 'test', address[place: 'test', pin]: 'test' })
  expect(status).toBe(404)
})

test('DELETE /shops/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${shop.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /shops/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${shop.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('DELETE /shops/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${shop.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /shops/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${shop.id}`)
  expect(status).toBe(401)
})

test('DELETE /shops/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
