import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy, uploadlogo, dashboard } from './controller'
import { schema } from './model'
export Shop, { schema } from './model'
//import { userSchema } from '../user/model'
//import User from '../user/model'


const router = new Router()
const { name, address, place, createdBy, print, bank, trn, email, logo, invoice_account, inventory_account } = schema.tree

/**
 * @api {post} /shops Create shop
 * @apiName CreateShop
 * @apiGroup Shop
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam name Shop's name.
 * @apiParam address[place Shop's address[place.
 * @apiParam pin] Shop's pin].
 * @apiSuccess {Object} shop Shop's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Shop not found.
 * @apiError 401 master access only.
 */
router.post('/',
  //master(),
  token({ required: true }),
  body({ name, address, place, invoice_account, createdBy, inventory_account, print, bank, trn, email, logo}),
  create)

/**
 * @api {get} /shops Retrieve shops
 * @apiName RetrieveShops
 * @apiGroup Shop
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of shops.
 * @apiSuccess {Object[]} rows List of shops.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: false }),
  query(),
  index)

/**
 * @api {get} /shops/:id Retrieve shop
 * @apiName RetrieveShop
 * @apiGroup Shop
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} shop Shop's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Shop not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {get} /shops/:id Retrieve shop
 * @apiName RetrieveShop
 * @apiGroup Shop
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} shop Shop's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Shop not found.
 * @apiError 401 user access only.
 */
router.get('/:id/dashboard',
  token({ required: true }),
  query({}),
  dashboard)

/**
 * @api {put} /shops/:id Update shop
 * @apiName UpdateShop
 * @apiGroup Shop
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam name Shop's name.
 * @apiParam address[place Shop's address[place.
 * @apiParam pin] Shop's pin].
 * @apiSuccess {Object} shop Shop's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Shop not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ name, address, place, invoice_account, print, bank, inventory_account, trn, email, logo }),
  update)

/**
 * @api {delete} /shops/:id Delete shop
 * @apiName DeleteShop
 * @apiGroup Shop
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Shop not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

  /**
 * @api {upload} /shops/logo/:id Delete shop
 * @apiName upload Shop Logo
 * @apiGroup Shop
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Shop not found.
 * @apiError 401 master access only.
 */
router.post('/:id/logo',
// token({ required: true }),
uploadlogo)

export default router
