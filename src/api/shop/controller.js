import { success, notFound } from '../../services/response/'
import { Shop } from '.'
import { Invoice } from '../invoice'
import { AccountType } from '../account-type'
import { Accounts } from '../accounts'
import { User } from '../user'

import mongoose from 'mongoose'
const fs = require('fs');
// import { User } from '.'

export const createOld = ({ bodymen: { body } }, res, next) =>
  Shop.create(body)
    .then((shop) => shop.view(true))
    .then(success(res, 201))
    .catch(next)


export const create = ({ bodymen: { body } }, res, next) => {
  Shop.create(body)
    .then((shop) => {
      const filePath = './apijson/accounts-type.json';
      const filePathAccounts = './apijson/accounts.json';
      const rawData = fs.readFileSync(filePath);
      const rawDataAccounts = fs.readFileSync(filePathAccounts);
      const accountTypesToInsert = JSON.parse(rawData);
      const accountsToInsert = JSON.parse(rawDataAccounts);
      const accountTypeMap = new Map();

      const accountTypePromises = accountTypesToInsert.map((accountType) => {
        accountType.shop = shop._id;
        accountType.createdBy = shop.createdBy;
        return AccountType.create(accountType);
      });
      return Promise.all(accountTypePromises)
        .then((insertedAccountTypes) => {

          insertedAccountTypes.forEach((accountType) => {
            accountTypeMap.set(accountType.name, accountType._id);
          });
          accountsToInsert.forEach((account) => {
            account.shop = shop._id;
            account.createdBy = shop.createdBy;
            const accountTypeName = account.account_type;
            if (accountTypeMap.has(accountTypeName)) {
              account.accounttype = accountTypeMap.get(accountTypeName);
            }
          });

          return Accounts.insertMany(accountsToInsert)
            .then((insertedAccounts) => {

              return {
                shop: shop,
                insertedAccounts,
              };
            });
        });
    })
    .then(({ shop, insertedAccounts }) => {


      User.findById(shop.createdBy)
      .then((user) => { 
        user.shops.forEach((shopItem) => {
          if (shopItem.sid !== shop._id) {
            shopItem.isactive = false;
          }
        });
        user.shops.push({
          accesstype: 'admin',
          isactive: true,
          sid: shop._id,
        });

        user.save().then((user) => user.view(true));
        
        // Further processing or response handling
        success(res, 201)(user);

       })
     
    })
    .catch(next);
}



export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Shop.count(query)
    .then(count => Shop.find(query, select, cursor).populate({ path: 'createdBy', select: 'name id' })
      .then((shops) => ({
        count,
        rows: shops.map((shop) => shop.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Shop.findById(params.id)
    .then(notFound(res))
    .then((shop) => shop ? shop.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Shop.findById(params.id)
    .then(notFound(res))
    .then((shop) => shop ? Object.assign(shop, body).save() : null)
    .then((shop) => shop ? shop.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Shop.findById(params.id)
    .then(notFound(res))
    .then((shop) => shop ? shop.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const uploadlogo = ({ bodymen: { body }, params }, res, next) =>
  Shop.findById(params.id)
    .then(notFound(res))
    //.then((shop) => shop ? Object.assign(shop, body).save() : null)
    .then((shop) => shop ? shop.view(true) : null)
    .then(success(res))
    .catch(next)
// get dashboard report items
export const dashboard = ({ querymen: { query, select, cursor }, params }, res, next) => {
  query._id = mongoose.Types.ObjectId(params.id);
  Shop.aggregate([
    { "$match": query },
    {
      $lookup: {
        from: Invoice.collection.name,
        let: { shop: "$_id" },

        pipeline: [
          { $addFields: { 'amt': { $sum: '$total' } } },
          { "$match": { "$expr": { "$eq": ["$shop", "$$shop"] } } }
        ],
        // localField: '_id',
        // foreignField: 'invoice',
        as: 'payments'
      }
    },
  ])
    .then(notFound(res))
    .then((shop) => ({
      count: shop.length,
      rows: shop
    }))
    .then(success(res))
    .catch(next)
}
