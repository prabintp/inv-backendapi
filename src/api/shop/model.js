import mongoose, { Schema } from 'mongoose'

const shopSchema = new Schema({
  name: {
    type: String
  },
  address: {
    type: String
  },
  email: {
    type: String
  },
  bank: {
    type: String
  },
  trn: {
    type: String
  },
  place: {
    type: String
  },
  logo: {
    type: String
  },
  print:{
    footer_text: String,
    terms_condition: String,
    quots_footer: String,
    purchase_footer: String
  },
  invoice_account:  {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
  },
  inventory_account:  {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
  },
  createdBy: {
		type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
	}
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }

  }
})

shopSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      address: this.address,
      place: this.place,
      trn: this.trn,
      bank: this.bank,
      print: this.print,
      email: this.email,
      logo: this.logo,
      invoice_account: this.invoice_account,
      createdBy: this.createdBy,
      inventory_account: this.inventory_account,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Shop', shopSchema)

export const schema = model.schema
export default model
