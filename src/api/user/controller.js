import { success, notFound } from '../../services/response/'
import { User } from '.'
import { sign } from '../../services/jwt'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.count(query)
    .then(count => User.find(query, select, cursor)
    //.where('name', 'test4')
      .then(users => ({
        rows: users.map((user) => user.view(true)),
        count
      }))
    )
    .then(success(res))
    .catch(next)

    export const showByShop = ({ querymen: { query, select, cursor } }, res, next) =>{
      if(!query['shops.sid']){
        res.status(401).json({
          valid: false,
          message: 'You can\'t get user\'s data without shop'
        })
        return null
      }
      else {
        User.count(query)
        .then(count => User.find(query, select, cursor)
        //.where('name', 'test4')
          .then(users => ({
            rows: users.map((user) => user.view(true)),
            count
          }))
        )
        .then(success(res))
        .catch(next)
      }
     
    }
   

export const show = ({ params }, res, next) =>
  User.findById(params.id).populate('shops.sid')
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const showMe1 = ({ user }, res) =>
  res.json(user.view(true))

  export const showMe = ({ user }, res) =>
    User.findById(user.id).populate('shops.sid')
    //.then(res.json(user.view(true)))
    .then(notFound(res))
    .then((user) => user ? user.view(true) : null)
    .then(success(res))

export const create = ({ bodymen: { body } }, res, next) =>
  User.create(body)
    .then(user => {
      sign(user.id)
        .then((token) => ({ token, user: user.view(true) }))
        .then(success(res, 201))
        .then(res => {
          console.log('--------user', user);
          console.log('--------res', res);
          if (user && typeof user.log === 'function') { 
             const data = {
               action: 'add-user',
               category: 'users',
               //createdBy: req.user.id,
               message: 'Updated user name',
          }
          console.log('inside user',data);
          return user.log(data)
      }
      }).catch(err => {
          console.log('Caught error while logging: ', err)
      })
    })
   
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          valid: false,
          param: 'email',
          message: 'email already registered'
        })
      } else {
        next(err)
      }
    })
   

export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      console.log(result+'gfgf');
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin) {
        res.status(401).json({
          valid: false,
          message: 'You can\'t change other user\'s data'
        })
        return null
      }
      return result
    })
    .then((user) => user ? Object.assign(user, body).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          valid: false,
          param: 'password',
          message: 'You can\'t change other user\'s password'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

    export const addShop = ({ bodymen: { body }, params, user }, res, next) =>
      User.findById(params.id === 'me' ? user.id : params.id)
        .then(notFound(res))
        .then((result) => {
          if (!result) return null
          console.log(result+ 'fdfdf' + user )
        /*  const isSelfUpdate = user.id === result.id
          if (!isSelfUpdate) {
            res.status(401).json({
              valid: false,
              param: 'password',
              message: 'You can\'t change other user\'s password'
            })
            return null
          }*/
          return result
        })
        .then((user) => {
          console.log(body.shops+'shopid');
            if (!user) return null
            user.shops.push({ sid: body.shops })
            return user.save()
        })
      //  .then((user) => user ? user.shops.push({sid: body.shops}).save() : null)
      //  .then((user) => user ? user.save() : null)
        .then((user) => user ? user.view(true) : null)
        .then(success(res))
        .catch(next)

export const destroy = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)
