import mongoose, { Schema } from 'mongoose'

const taxSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String
  },
  code: {
    type: String
  },
  rate: {
    type: String
  },
  type: {
    type: String
  },
  shop: {
    type: Schema.ObjectId,
    ref: 'Shop',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

taxSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      name: this.name,
      code: this.code,
      rate: this.rate,
      type: this.type,
      shop: this.shop.view(),
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Tax', taxSchema)

export const schema = model.schema
export default model
