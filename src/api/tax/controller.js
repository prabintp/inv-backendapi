import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Tax } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Tax.create({ ...body, user })
    .then((tax) => tax.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Tax.count(query)
    .then(count => Tax.find(query, select, cursor)
      .populate('user')
      .then((taxes) => ({
        count,
        rows: taxes.map((tax) => tax.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Tax.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((tax) => tax ? tax.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Tax.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((tax) => tax ? Object.assign(tax, body).save() : null)
    .then((tax) => tax ? tax.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  Tax.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((tax) => tax ? tax.remove() : null)
    .then(success(res, 204))
    .catch(next)
