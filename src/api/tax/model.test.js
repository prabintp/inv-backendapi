import { Tax } from '.'
import { User } from '../user'

let user, tax

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  tax = await Tax.create({ user, name: 'test', code: 'test', rate: 'test', type: 'test', shop: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = tax.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tax.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.name).toBe(tax.name)
    expect(view.code).toBe(tax.code)
    expect(view.rate).toBe(tax.rate)
    expect(view.type).toBe(tax.type)
    expect(view.shop).toBe(tax.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = tax.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tax.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.name).toBe(tax.name)
    expect(view.code).toBe(tax.code)
    expect(view.rate).toBe(tax.rate)
    expect(view.type).toBe(tax.type)
    expect(view.shop).toBe(tax.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
