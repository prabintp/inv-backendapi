import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Tax } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, tax

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  tax = await Tax.create({ user })
})

test('POST /taxes 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, name: 'test', code: 'test', rate: 'test', type: 'test', shop: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.rate).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /taxes 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /taxes 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
  expect(typeof body.rows[0].user).toEqual('object')
})

test('GET /taxes 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /taxes/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${tax.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tax.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /taxes/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${tax.id}`)
  expect(status).toBe(401)
})

test('GET /taxes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /taxes/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${tax.id}`)
    .send({ access_token: userSession, name: 'test', code: 'test', rate: 'test', type: 'test', shop: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tax.id)
  expect(body.name).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.rate).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /taxes/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${tax.id}`)
    .send({ access_token: anotherSession, name: 'test', code: 'test', rate: 'test', type: 'test', shop: 'test' })
  expect(status).toBe(401)
})

test('PUT /taxes/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${tax.id}`)
  expect(status).toBe(401)
})

test('PUT /taxes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, name: 'test', code: 'test', rate: 'test', type: 'test', shop: 'test' })
  expect(status).toBe(404)
})

test('DELETE /taxes/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${tax.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /taxes/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${tax.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /taxes/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${tax.id}`)
  expect(status).toBe(401)
})

test('DELETE /taxes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})
