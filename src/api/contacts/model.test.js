import { Contacts } from '.'
import { User } from '../user'

let user, contacts

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  contacts = await Contacts.create({ created_by: user, name: 'test', email: 'test', shop: 'test', saddress: 'test', baddress: 'test', phone: 'test', mobile: 'test', type: 'test', contact: 'test', company: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = contacts.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(contacts.id)
    expect(typeof view.created_by).toBe('object')
    expect(view.created_by.id).toBe(user.id)
    expect(view.name).toBe(contacts.name)
    expect(view.email).toBe(contacts.email)
    expect(view.shop).toBe(contacts.shop)
    expect(view.saddress).toBe(contacts.saddress)
    expect(view.baddress).toBe(contacts.baddress)
    expect(view.phone).toBe(contacts.phone)
    expect(view.mobile).toBe(contacts.mobile)
    expect(view.type).toBe(contacts.type)
    expect(view.contact).toBe(contacts.contact)
    expect(view.company).toBe(contacts.company)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = contacts.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(contacts.id)
    expect(typeof view.created_by).toBe('object')
    expect(view.created_by.id).toBe(user.id)
    expect(view.name).toBe(contacts.name)
    expect(view.email).toBe(contacts.email)
    expect(view.shop).toBe(contacts.shop)
    expect(view.saddress).toBe(contacts.saddress)
    expect(view.baddress).toBe(contacts.baddress)
    expect(view.phone).toBe(contacts.phone)
    expect(view.mobile).toBe(contacts.mobile)
    expect(view.type).toBe(contacts.type)
    expect(view.contact).toBe(contacts.contact)
    expect(view.company).toBe(contacts.company)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
