import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Contacts } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, contacts

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  contacts = await Contacts.create({ created_by: user })
})

test('POST /contacts 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, name: 'test', email: 'test', shop: 'test', saddress: 'test', baddress: 'test', phone: 'test', mobile: 'test', type: 'test', contact: 'test', company: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(body.saddress).toEqual('test')
  expect(body.baddress).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.mobile).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.contact).toEqual('test')
  expect(body.company).toEqual('test')
  expect(typeof body.created_by).toEqual('object')
})

test('POST /contacts 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /contacts 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
  expect(typeof body.rows[0].created_by).toEqual('object')
})

test('GET /contacts 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /contacts/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${contacts.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(contacts.id)
  expect(typeof body.created_by).toEqual('object')
})

test('GET /contacts/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${contacts.id}`)
  expect(status).toBe(401)
})

test('GET /contacts/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /contacts/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${contacts.id}`)
    .send({ access_token: userSession, name: 'test', email: 'test', shop: 'test', saddress: 'test', baddress: 'test', phone: 'test', mobile: 'test', type: 'test', contact: 'test', company: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(contacts.id)
  expect(body.name).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(body.saddress).toEqual('test')
  expect(body.baddress).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.mobile).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.contact).toEqual('test')
  expect(body.company).toEqual('test')
  expect(typeof body.created_by).toEqual('object')
})

test('PUT /contacts/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${contacts.id}`)
    .send({ access_token: anotherSession, name: 'test', email: 'test', shop: 'test', saddress: 'test', baddress: 'test', phone: 'test', mobile: 'test', type: 'test', contact: 'test', company: 'test' })
  expect(status).toBe(401)
})

test('PUT /contacts/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${contacts.id}`)
  expect(status).toBe(401)
})

test('PUT /contacts/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, name: 'test', email: 'test', shop: 'test', saddress: 'test', baddress: 'test', phone: 'test', mobile: 'test', type: 'test', contact: 'test', company: 'test' })
  expect(status).toBe(404)
})

test('DELETE /contacts/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${contacts.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /contacts/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${contacts.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /contacts/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${contacts.id}`)
  expect(status).toBe(401)
})

test('DELETE /contacts/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})
