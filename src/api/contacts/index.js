import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Contacts, { schema } from './model'

const router = new Router()
const { name, email, shop, saddress, baddress, phone, mobile, type, contact, company, trn } = schema.tree

/**
 * @api {post} /contacts Create contacts
 * @apiName CreateContacts
 * @apiGroup Contacts
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Contacts's name.
 * @apiParam email Contacts's email.
 * @apiParam shop Contacts's shop.
 * @apiParam saddress Contacts's saddress.
 * @apiParam baddress Contacts's baddress.
 * @apiParam phone Contacts's phone.
 * @apiParam mobile Contacts's mobile.
 * @apiParam type Contacts's type.
 * @apiParam contact Contacts's contact.
 * @apiParam company Contacts's company.
 * @apiSuccess {Object} contacts Contacts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Contacts not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ name, email, shop, saddress, baddress, phone, mobile, type, contact, company, trn }),
  create)

/**
 * @api {get} /contacts Retrieve contacts
 * @apiName RetrieveContacts
 * @apiGroup Contacts
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of contacts.
 * @apiSuccess {Object[]} rows List of contacts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop: {
    paths: ['shop']
   }
  }),
  index)

/**
 * @api {get} /contacts/:id Retrieve contacts
 * @apiName RetrieveContacts
 * @apiGroup Contacts
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} contacts Contacts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Contacts not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /contacts/:id Update contacts
 * @apiName UpdateContacts
 * @apiGroup Contacts
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Contacts's name.
 * @apiParam email Contacts's email.
 * @apiParam shop Contacts's shop.
 * @apiParam saddress Contacts's saddress.
 * @apiParam baddress Contacts's baddress.
 * @apiParam phone Contacts's phone.
 * @apiParam mobile Contacts's mobile.
 * @apiParam type Contacts's type.
 * @apiParam contact Contacts's contact.
 * @apiParam company Contacts's company.
 * @apiSuccess {Object} contacts Contacts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Contacts not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ name, email, saddress, baddress, phone, mobile, type, contact, company, trn }),
  update)

/**
 * @api {delete} /contacts/:id Delete contacts
 * @apiName DeleteContacts
 * @apiGroup Contacts
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Contacts not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
