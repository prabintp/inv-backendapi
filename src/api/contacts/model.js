import mongoose, { Schema } from 'mongoose'
const typ = ['c', 'v', 's']

const contactsSchema = new Schema({
  created_by: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  trn: {
	 type: String
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop'
  },
  saddress: {
    type: String
  },
  baddress: {
    type: String
  },
  phone: {
    type: String
  },
  mobile: {
    type: String
  },
  type: {
    type: String,
    enum: typ,
    default: 'c'
  },
  contact: {
    type: String
  },
  company: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

contactsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      created_by: this.created_by.view(full),
      name: this.name,
      email: this.email,
      shop: this.shop,
      saddress: this.saddress,
      baddress: this.baddress,
      phone: this.phone,
      mobile: this.mobile,
      type: this.type,
      contact: this.contact,
      company: this.company,
	    trn: this.trn,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Contacts', contactsSchema)

export const schema = model.schema
export default model
