import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Contacts } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Contacts.create({ ...body, created_by: user })
    .then((contacts) => contacts.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Contacts.count(query)
    .then(count => Contacts.find(query, select, cursor)
      .populate('created_by')
      .then((contacts) => ({
        count,
        rows: contacts.map((contacts) => contacts.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Contacts.findById(params.id)
    .populate('created_by')
    .then(notFound(res))
    .then((contacts) => contacts ? contacts.view(true) : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Contacts.findById(params.id)
    .populate('created_by')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'created_by'))
    .then((contacts) => contacts ? Object.assign(contacts, body).save() : null)
    .then((contacts) => contacts ? contacts.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  Contacts.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'created_by'))
    .then((contacts) => contacts ? contacts.remove() : null)
    .then(success(res, 204))
    .catch(next)
