import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy, showbyaccount, ccallback, byAccountGroup } from './controller'
import { schema } from './model'
export Transaction, { schema } from './model'

const router = new Router()
const { txnType, amount, account, txn_date, lineAccount, customer, supplier, tax, paymentMethod, currencyType, invoice, bill, shop, note } = schema.tree

/**
 * @api {post} /transactions Create transaction
 * @apiName CreateTransaction
 * @apiGroup Transaction
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam txnType Transaction's txnType.(payment-any payment made aganist invoice to cash account,
 * salesInvoice- any invoice amount with out tax, to sales account,
 * item- opening balance of item to Opening Balance Adjustments account,
 * 
 * 
 * @apiParam amount Transaction's amount.
 * @apiParam account Transaction's account.
 * @apiParam lineAccount Transaction's lineAccount.
 * @apiParam customer Transaction's customer.
 * @apiParam supplier Transaction's supplier.
 * @apiParam tax Transaction's tax.
 * @apiParam paymentMethod Transaction's paymentMethod.
 * @apiParam currencyType Transaction's currencyType.
 * @apiParam invoice Transaction's invoice.
 * @apiParam bill Transaction's bill.
 * @apiParam shop Transaction's shop.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ txnType, amount, account, txn_date, lineAccount, customer, supplier, tax, paymentMethod, currencyType, invoice, bill, shop, note }),
  create)

/**
 * @api {get} /transactions Retrieve transactions
 * @apiName RetrieveTransactions
 * @apiGroup Transaction
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of transactions.
 * @apiSuccess {Object[]} rows List of transactions.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query({
    limit: { max: 2000, default: 2000 },
    txnType,
    shop: {
      paths: ['shop']
    }
  }),
  index, ccallback)

/**
* @api {get} /transactions Retrieve transactions
* @apiName RetrieveTransactions by category
* @apiGroup Transaction
* @apiPermission user
* @apiParam {String} access_token user access token.
* @apiUse listParams
* @apiSuccess {Number} count Total amount of transactions.
* @apiSuccess {Object[]} rows List of transactions.
* @apiError {Object} 400 Some parameters may contain invalid values.
* @apiError 401 user access only.
*/
router.get('/bycategory',
  token({ required: true }),
  query({
    limit: { max: 2000, default: 2000 },
    txnType,
    shop: {
      paths: ['shop']
    }
  }),
  byAccountGroup, ccallback)

/**
 * @api {get} /transactions/:id Retrieve transaction
 * @apiName RetrieveTransaction
 * @apiGroup Transaction
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show, ccallback)

/**
 * @api {put} /transactions/:id Update transaction
 * @apiName UpdateTransaction
 * @apiGroup Transaction
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam txnType Transaction's txnType.
 * @apiParam amount Transaction's amount.
 * @apiParam account Transaction's account.
 * @apiParam lineAccount Transaction's lineAccount.
 * @apiParam customer Transaction's customer.
 * @apiParam supplier Transaction's supplier.
 * @apiParam tax Transaction's tax.
 * @apiParam paymentMethod Transaction's paymentMethod.
 * @apiParam currencyType Transaction's currencyType.
 * @apiParam invoice Transaction's invoice.
 * @apiParam bill Transaction's bill.
 * @apiParam shop Transaction's shop.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ txnType, amount, account, txn_date, lineAccount, customer, supplier, tax, paymentMethod, currencyType, invoice, bill, note }),
  update)

/**
 * @api {delete} /transactions/:id Delete transaction
 * @apiName DeleteTransaction
 * @apiGroup Transaction
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Transaction not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

/**
 * @api {get} /transactions/byaccount/:id Retrieve transaction
 * @apiName RetrieveTransaction based on account
 * @apiGroup Transaction
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 * @apiError 401 user access only.
 */
router.get('/byaccount/:id',
  token({ required: true }),
  query({
    limit: { max: 2000, default: 2000 },
    shop
  }),
  showbyaccount
)

export default router
