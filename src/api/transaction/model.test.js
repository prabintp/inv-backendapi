import { Transaction } from '.'
import { User } from '../user'

let user, transaction

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  transaction = await Transaction.create({ user, txnType: 'test', amount: 'test', account: 'test', lineAccount: 'test', customer: 'test', supplier: 'test', tax: 'test', paymentMethod: 'test', currencyType: 'test', invoice: 'test', bill: 'test', shop: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = transaction.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transaction.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.txnType).toBe(transaction.txnType)
    expect(view.amount).toBe(transaction.amount)
    expect(view.account).toBe(transaction.account)
    expect(view.lineAccount).toBe(transaction.lineAccount)
    expect(view.customer).toBe(transaction.customer)
    expect(view.supplier).toBe(transaction.supplier)
    expect(view.tax).toBe(transaction.tax)
    expect(view.paymentMethod).toBe(transaction.paymentMethod)
    expect(view.currencyType).toBe(transaction.currencyType)
    expect(view.invoice).toBe(transaction.invoice)
    expect(view.bill).toBe(transaction.bill)
    expect(view.shop).toBe(transaction.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = transaction.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transaction.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.txnType).toBe(transaction.txnType)
    expect(view.amount).toBe(transaction.amount)
    expect(view.account).toBe(transaction.account)
    expect(view.lineAccount).toBe(transaction.lineAccount)
    expect(view.customer).toBe(transaction.customer)
    expect(view.supplier).toBe(transaction.supplier)
    expect(view.tax).toBe(transaction.tax)
    expect(view.paymentMethod).toBe(transaction.paymentMethod)
    expect(view.currencyType).toBe(transaction.currencyType)
    expect(view.invoice).toBe(transaction.invoice)
    expect(view.bill).toBe(transaction.bill)
    expect(view.shop).toBe(transaction.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
