import mongoose, { Schema } from 'mongoose';
const TXNTYPE = ['payment','expense','deposit', 'bill', 'bill_payment', 'invoice', 'invoice_payment', 'opening_balance', 'inventory'];
const PMETHOD = ['cash', 'cheque', 'card', 'other'];
mongoose.plugin(require('../..//utils/diff-plugin'));

const transactionSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  txnType: {
    type: String,
    enum: TXNTYPE,
    default: 'payment' 
  },
  amount: {
    type: Number,
    required: true
  },
  debit: {
    type: Number
  },
  credit: {
    type: Number
  },
  account:  {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts',
    required: true
  },
  lineAccount:  {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
  },
  customer:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contacts'
  },
  supplier: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contacts'
  },
  tax: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tax'
  },
  paymentMethod: {
    type: String,
    enum: PMETHOD,
    default: 'other' 
  },
  currencyType: {
    type: String
  },
  item_id:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Items'
  },
  txn_date: { year: Number, month: Number, day: Number },
  invoice: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Invoice'
  },
  bill: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Invoice'
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop',
    required: true
  },
  note: {
    type: String
  },
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

// Getter
transactionSchema.path('amount').get(function(num) {
  return (num / 100).toFixed(3);
});

// Setter
transactionSchema.path('amount').set(function(num) {
  return num * 100;
});

transactionSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      txnType: this.txnType,
      amount: this.amount,
      debit: this.debit,
      credit: this.credit,
      account: this.account,
      lineAccount: this.lineAccount,
      customer: this.customer,
      supplier: this.supplier,
      tax: this.tax,
      paymentMethod: this.paymentMethod,
      currencyType: this.currencyType,
      invoice: this.invoice,
      bill: this.bill,
      txn_date: this.txn_date,
      shop: this.shop,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      note: this.note,
      item_id: this.item_id,
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Transaction', transactionSchema)

export const schema = model.schema
export default model
