import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Transaction } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, adminSession, transaction

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  adminSession = signSync(admin.id)
  transaction = await Transaction.create({ user })
})

test('POST /transactions 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, txnType: 'test', amount: 'test', account: 'test', lineAccount: 'test', customer: 'test', supplier: 'test', tax: 'test', paymentMethod: 'test', currencyType: 'test', invoice: 'test', bill: 'test', shop: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.txnType).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.account).toEqual('test')
  expect(body.lineAccount).toEqual('test')
  expect(body.customer).toEqual('test')
  expect(body.supplier).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.paymentMethod).toEqual('test')
  expect(body.currencyType).toEqual('test')
  expect(body.invoice).toEqual('test')
  expect(body.bill).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /transactions 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /transactions 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
  expect(typeof body.rows[0].user).toEqual('object')
})

test('GET /transactions 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /transactions/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${transaction.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(transaction.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /transactions/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${transaction.id}`)
  expect(status).toBe(401)
})

test('GET /transactions/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /transactions/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${transaction.id}`)
    .send({ access_token: userSession, txnType: 'test', amount: 'test', account: 'test', lineAccount: 'test', customer: 'test', supplier: 'test', tax: 'test', paymentMethod: 'test', currencyType: 'test', invoice: 'test', bill: 'test', shop: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(transaction.id)
  expect(body.txnType).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.account).toEqual('test')
  expect(body.lineAccount).toEqual('test')
  expect(body.customer).toEqual('test')
  expect(body.supplier).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.paymentMethod).toEqual('test')
  expect(body.currencyType).toEqual('test')
  expect(body.invoice).toEqual('test')
  expect(body.bill).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /transactions/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${transaction.id}`)
    .send({ access_token: anotherSession, txnType: 'test', amount: 'test', account: 'test', lineAccount: 'test', customer: 'test', supplier: 'test', tax: 'test', paymentMethod: 'test', currencyType: 'test', invoice: 'test', bill: 'test', shop: 'test' })
  expect(status).toBe(401)
})

test('PUT /transactions/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${transaction.id}`)
  expect(status).toBe(401)
})

test('PUT /transactions/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, txnType: 'test', amount: 'test', account: 'test', lineAccount: 'test', customer: 'test', supplier: 'test', tax: 'test', paymentMethod: 'test', currencyType: 'test', invoice: 'test', bill: 'test', shop: 'test' })
  expect(status).toBe(404)
})

test('DELETE /transactions/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${transaction.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /transactions/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${transaction.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /transactions/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${transaction.id}`)
  expect(status).toBe(401)
})

test('DELETE /transactions/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
