import { success, notFound, authorOrAdmin } from '../../services/response/'
import mongoose from 'mongoose'
import { Transaction } from '.'
import { Accounts } from '../accounts'

export const createOld = ({ user, bodymen: { body } }, res, next) =>
  Transaction.create({ ...body, user })
    .then((transaction) => transaction.view(true))
  //  .then(success(res, 201))
    .then(invoice => {
      console.log('outtrn',invoice);
      if (invoice && typeof invoice.log === 'function') {
        console.log('intrn',invoice);
        const data = {
          action: 'create',
          category: invoice.txnType,
          createdBy: invoice.createdBy,
          shop: invoice.shop,
          itemId: invoice._id,
          itemName: invoice.invoice,
          message: `Transaction created`,
        }
        return invoice.log(data)
      }
    })
    .catch(next)

    export const create = ({ user, bodymen: { body } }, res, next) =>
    Transaction.create({ ...body, user })
    .then((transaction) => {
      var inv = transaction.view(true);
      res.status(201).json(inv);
      return transaction;
    })
    .then(transaction => {
      if (transaction && typeof transaction.log === 'function') {
        const data = {
          action: 'added',
          category: 'payments',
          createdBy: transaction.user.id,
          shop: transaction.shop,
          itemId: transaction._id,
          itemName: transaction.invoice ? transaction.invoice : 'bill',
          value: transaction.amount,
          message: `Transaction created`,
        }
        return transaction.log(data)
      }
    }).catch(err => {
      console.log('Caught error while logging: ', err)
    })

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Transaction.count(query)
    .then(count => Transaction.find(query, select, cursor)
      .populate('user')
      .populate('lineAccount')
      .populate('account')
      .then((transactions) => ({
        count,
        rows: transactions.map((transaction) => transaction.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Transaction.findById(params.id)
    .populate('user')
    .populate('invoice')
    .populate({ path: 'customer', select: 'name id ' })
    .then(notFound(res))
    .then((transaction) => transaction ? transaction.view() : null)
    .then(success(res))
    .then(next)
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Transaction.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((transaction) => transaction ? Object.assign(transaction, body).save() : null)
    .then((transaction) => transaction ? transaction.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Transaction.findById(params.id)
    .then(notFound(res))
    .then((transaction) => transaction ? transaction.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const showbyaccount = ({ querymen: { query, select, cursor }, params }, res, next) => {
  var accountid = params.id;
  delete query.account;
  var globalAmount = 0;
  var or = Object.assign({}, {
    "$or": [{
      "lineAccount": accountid
    }, {
      "account": accountid
    }]
  }, query);
  if (query.shop) {
    query.shop = mongoose.Types.ObjectId(query.shop);
  }
  var fromAccount = Object.assign({},
    {
      "account": mongoose.Types.ObjectId(accountid)
    }, query);

  var toAccount = Object.assign({},
    {
      "lineAccount": mongoose.Types.ObjectId(accountid)
    }, query);


  ;


  Transaction.aggregate([{
    $facet: {
      "debit": [
        { $match: fromAccount },
        // {$limit: 1},
        { $group: { _id: "$account", total: { $sum: "$amount" } } },
        { $project: { amount: { $divide: ["$total", 100] } } }
      ],
      "credit": [
        { $match: toAccount },
        // {$limit: 1},
        { $group: { _id: "$lineAccount", total: { $sum: "$amount" } } },
        { $project: { amount: { $divide: ["$total", 100] } } }
      ]


    }
  }

  ]).then(aggregate =>

    Transaction.count(or).then((count) => ({
      aggregate,
      count
    })


    ))


    // aggregate,
    .then((r) => Transaction.find(or, select, cursor)
      .populate('user')
      .populate('invoice')
      .then((transactions) => ({
        account: r.aggregate ? r.aggregate[0] : [],
        count: r.count,
        rows: transactions.map((transaction) =>
          transaction.view()

        ),

      }))
    )
    .then(success(res))
    .catch(next)
}

export const showbyaccountOld = ({ querymen: { query, select, cursor }, params }, res, next) => {
  var accountid = params.id;
  delete query.account;
  var globalAmount = 0;
  var or = Object.assign({}, {
    "$or": [{
      "lineAccount": accountid
    }, {
      "account": accountid
    }]
  }, query);


  // query.push(or);
  console.log(or);

  Transaction.count(or)

    .then(count => Transaction.find(or, select, cursor)
      .populate('user')
      .populate('invoice')
      .then((transactions) => ({
        count,
        rows: transactions.map((doc) =>
          doc.view()
          //  {
          //   globalAmount += doc.amount;
          //  return Object.assign(doc, { amount: globalAmount });
          // }
        ),
        total: transactions.map((doc) =>
        // transaction.view()
        {
          globalAmount += doc.amount;
          return Object.assign(doc, { amount: globalAmount });
        }
        )
      }))
    )
    .then(success(res))
    .catch(next)
}

export const ccallback = (res) => {
  console.log(res + 'rrr');
}


export const byAccountGroup = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log('query',query);
  console.log('query',select);
  console.log('query',cursor);
  query.shop = mongoose.Types.ObjectId(query.shop);
  Transaction.aggregate( [ 
    { 
      $match: query
    },
    { $sort: cursor.sort },
    { $limit: cursor.limit },
    { 
    $group : { 
      _id : "$lineAccount",
      totalAmount: { $sum: {
        $divide: ["$amount", 100]
      } },
      count: { $sum: 1 }
    }
  },
  {
    $lookup: {
    from: Accounts.collection.name, 
    localField: "_id", 
    foreignField: "_id", 
    as: "category"}
  }, 

] )
  

 /* Transaction.count(query)
    .then(count => Transaction.find(query, select, cursor)
      .populate('user')
      .populate('lineAccount')
      .populate('account')
      .then((transactions) => ({
        count,
        rows: transactions.map((transaction) => transaction.view())
      }))
    )*/
    .then(success(res))
    .catch(next)
}
  