import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy, createMany } from './controller'
import { schema } from './model'
export Items, { schema } from './model'

const router = new Router()
const { name, sku, desc, quantity, unit, unitprice, actualprice, category, expiredate, isInventoryItem,inventoryCostAccount, inventorySalesAccount, isTaxable,  taxType, vendor, status, isService, shop, createdBy, notes, inventoryAccount, quantityRate } = schema.tree

/**
 * @api {post} /items Create items
 * @apiName CreateItems
 * @apiGroup Items
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam name Items's name.
 * @apiParam sku Items's sku.
 * @apiParam desc Items's desc.
 * @apiParam quantity Items's quantity.
 * @apiParam unit Items's unit.
 * @apiParam unitprice Items's unitprice.
 * @apiParam sellingprice Items's sellingprice.
 * @apiParam category Items's category.
 * @apiParam expiredate Items's expiredate.
 * @apiParam tax Items's tax.
 * @apiParam vendor Items's vendor.
 * @apiParam status Items's status.
 * @apiParam isservice Items's isservice.
 * @apiParam shop Items's shop.
 * @apiParam createdby Items's createdby.
 * @apiSuccess {Object} items Items's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Items not found.
 * @apiError 401 master access only.
 */
router.post('/',
token({ required: true }),
  body({ name, sku, desc, quantity, quantityRate, unit, unitprice, actualprice, category, expiredate,isInventoryItem, inventorySalesAccount, inventoryCostAccount, isTaxable,  taxType, vendor, status, isService, shop, createdBy, notes, inventoryAccount }),
  create)

  router.post('/insertMany',
    createMany)

/**
 * @api {get} /items Retrieve items
 * @apiName RetrieveItems
 * @apiGroup Items
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of items.
 * @apiSuccess {Object[]} rows List of items.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  token({ required: true }),
  query({
    limit: { max: 4500, default:4500 },
    shop: {
    paths: ['shop']
   }
  }),
  index)

/**
 * @api {get} /items/:id Retrieve items
 * @apiName RetrieveItems
 * @apiGroup Items
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} items Items's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Items not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /items/:id Update items
 * @apiName UpdateItems
 * @apiGroup Items
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam name Items's name.
 * @apiParam sku Items's sku.
 * @apiParam desc Items's desc.
 * @apiParam quantity Items's quantity.
 * @apiParam unit Items's unit.
 * @apiParam unitprice Items's unitprice.
 * @apiParam sellingprice Items's sellingprice.
 * @apiParam category Items's category.
 * @apiParam expiredate Items's expiredate.
 * @apiParam tax Items's tax.
 * @apiParam vendor Items's vendor.
 * @apiParam status Items's status.
 * @apiParam isservice Items's isservice.
 * @apiParam shop Items's shop.
 * @apiParam createdby Items's createdby.
 * @apiSuccess {Object} items Items's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Items not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
    token({ required: true }),
  body({ name, sku, desc, quantity, quantityRate, unit, unitprice, actualprice, category, expiredate, inventorySalesAccount, isInventoryItem, inventoryCostAccount, isTaxable,  taxType, vendor, status, isService, shop, createdBy, notes, inventoryAccount }),
  update)

/**
 * @api {delete} /items/:id Delete items
 * @apiName DeleteItems
 * @apiGroup Items
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Items not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
    token({ required: true }),
  destroy)

export default router
