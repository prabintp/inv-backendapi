import { success, notFound } from '../../services/response/'
import { Items } from '.'
import { Stock } from '../stock';
import { Transaction } from '../transaction';
import { Accounts } from '../accounts';

export const createOld = ({ bodymen: { body } }, res, next) =>
  Items.create(body)
    .then((items) => items.view(true))
    .then(success(res, 201))
    .catch(next)



export const create = ({ bodymen: { body } }, res, next) =>
  Items.create(body)
    .then((items) => {
      return Accounts.findOne({ shop: items.shop, name: "Opening Balance Adjustments" })
        .then((account) => {
          if (!account) {
            return {
              items: items.view()
            };
          }
          // Transaction type Opening _balance credit to Opening Balance adjustment and debit to Inventory Asset
          const amt = items.quantity * items.quantityRate;
          const transactionData = [{
            account: items.inventoryAccount,
            amount: amt,
            credit: amt,
            shop: items.shop,
            txnType: 'opening_balance',
            user: items.createdBy,
            item_id: items.id,
          },
          {
            account: account.id,
            amount: amt,
            debit: amt,
            shop: items.shop,
            txnType: 'opening_balance',
            user: items.createdBy,
            item_id: items.id,
          }
          ];
          return Transaction.create(transactionData)
            .then((transaction) => {
              console.log('amtinv', items.isInventoryItem);
              if (items.isInventoryItem) {

                // Create a new stock document
                const stockData = {
                  product_id: items._id,
                  quantity: items.quantity, // Set initial quantity to 0
                  quantityRate: items.quantityRate,
                  source: "i",
                  shop: items.shop,
                  createdBy: items.createdBy,
                };
                // Insert the stock document
                return Stock.create(stockData)
                  .then((stocks) => ({
                    items: items.view(),
                    stock: stocks.view()
                  }))
                  .then(success(res, 201))
                  .catch(next);
              }
              else {
                return {
                  items: items.view()
                };
              }
            })
        })
    })
    .catch(next);


export const createMany = (req, res, next) =>
  Items.insertMany(req.body)
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Items.count(query)
    .then(count => Items.find(query, select, cursor).populate({ path: 'category', select: 'name id' })
      .then((items) => ({
        count,
        rows: items.map((items) => items.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Items.findById(params.id).populate({ path: 'category', select: 'name id' })
    .then(notFound(res))
    .then((items) => items ? items.view() : null)
    .then(success(res))
    .catch(next)

export const updateOld = ({ bodymen: { body }, params }, res, next) =>
  Items.findById(params.id)
    .then(notFound(res))
    .then((items) => items ? Object.assign(items, body).save() : null)
    .then((items) => items ? items.view(true) : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Items.findById(params.id)
    .then(notFound(res))
    .then((items) => {
      if (items) {
        // Update items with the new body
        Object.assign(items, body);
        return items.save()
          .then((updatedItems) => {




            return Accounts.findOne({ shop: items.shop, name: "Opening Balance Adjustments" })
              .then((account) => {
                if (!account) {
                  return {
                    items: items.view()
                  };
                }

                // Delete existing stock documents with matching product_id and source
                return Transaction.deleteMany({ item_id: updatedItems._id, txnType: 'opening_balance', shop: updatedItems.shop })
                  .then(() => {



                    const amt = updatedItems.quantity * updatedItems.quantityRate;
                    const transactionData = [{
                      account: updatedItems.inventoryAccount,
                      amount: amt,
                      credit: amt,
                      shop: updatedItems.shop,
                      txnType: 'opening_balance',
                      user: updatedItems.createdBy,
                      item_id: updatedItems.id,
                    },
                    {
                      account: account.id,
                      amount: amt,
                      debit: amt,
                      shop: updatedItems.shop,
                      txnType: 'opening_balance',
                      user: updatedItems.createdBy,
                      item_id: updatedItems.id,
                    }
                    ];

                    return Transaction.create(transactionData)
            .then((transactions) => {
                     // Delete existing stock documents with matching product_id and source
                     return Stock.deleteMany({ product_id: updatedItems._id, source: "i" })
                     .then(() => {



                       if (updatedItems.isInventoryItem) {
                         // Create a new stock document
                         const stockData = {
                           product_id: updatedItems._id,
                           quantity: updatedItems.quantity,
                           quantityRate: updatedItems.quantityRate,
                           source: "i",
                           shop: updatedItems.shop
                         };

                         // Insert the stock document
                         return Stock.create(stockData)
                           .then((stocks) => ({
                             items: updatedItems.view(true),
                             stock: stocks.view(),
                             transaction: transactions.map((transaction) => transaction.view())
                           }));


                       } else {
                         return {
                           items: updatedItems.view(true)
                         };
                       }

                     });//end stock delete

            })// transacton create end

                   

                  }) // end transaction delete





              })



          });
      } else {
        return null;
      }
    })
    .then(success(res))
    .catch(next);


export const destroy = ({ params }, res, next) =>
  Items.findById(params.id)
    .then(notFound(res))
    .then((items) => items ? items.remove() : null)
    .then(success(res, 204))
    .catch(next)
