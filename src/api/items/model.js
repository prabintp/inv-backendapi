import mongoose, { Schema } from 'mongoose'
// inveentory quantity type -  initial , adjust, sales, purchase
const qtyType = ['i', 'a', 's', 'p' ]

const itemsSchema = new Schema({
  name: {
    type: String
  },
  sku: {
    type: String,
    trim: true,
   // validate: {validator: isSKUUnique, msg: 'SKU already exist'}
  },
  desc: {
    type: String
  },
  notes: {
    type: String
  },
  quantity: {
    type: Number
  },
  quantityRate: {
    type: Number
  },
  unit: {
    type: String
  },
  unitprice: {
    type: Number
  },
  actualprice: {
    type: Number
  },
  isInventoryItem: {
    type: Boolean,
    default: false
  },
  inventory:
     [{
      type: {
        type: String,
        enum: qtyType,
        default: 'i'
      },
      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      },
      quantity:{
        type: Number
      }
    }],

  inventoryAccount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
    
  },
  //purchase account
  inventoryCostAccount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
   
  },
  inventorySalesAccount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Accounts'
   
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  },
  expiredate: {
    type: String
  },
  isTaxable: {
    type: Boolean,
    default: false 
  },
  taxType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tax'
  },
  vendor: {
    type: String
  },
  status: {
    type: String
  },
  isService: {
    type: Boolean,
    default: false
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop'
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

function isSKUUnique(value, done){
  if(value){
    mongoose.models['Items'].count({ _id: {'$ne': this._id }, SKU: value }, function (err, count) {
     if (err) {
       return done(err);
     }
     // If `count` is greater than zero, "invalidate"
     done(!count);
    // return !count;
   });
  }
}

itemsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      sku: this.sku,
      desc: this.desc,
      quantity: this.quantity,
      quantityRate: this.quantityRate,
      unit: this.unit,
      unitprice: this.unitprice,
      actualprice: this.actualprice,
      category: this.category,
      expiredate: this.expiredate,
      taxType: this.taxType,
      vendor: this.vendor,
      status: this.status,
      isService: this.isService,
      shop: this.shop,
      createdBy: this.createdBy,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      notes: this.notes,
      isInventoryItem: this.isInventoryItem,
      inventoryCostAccount: this.inventoryCostAccount,
      inventorySalesAccount: this.inventorySalesAccount,
      inventoryAccount: this.inventoryAccount,
      isTaxable: this.isTaxable
      


    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Items', itemsSchema)

export const schema = model.schema
export default model
