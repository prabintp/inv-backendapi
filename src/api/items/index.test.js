import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Items } from '.'

const app = () => express(apiRoot, routes)

let items

beforeEach(async () => {
  items = await Items.create({})
})

test('POST /items 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, name: 'test', sku: 'test', desc: 'test', quantity: 'test', unit: 'test', unitprice: 'test', sellingprice: 'test', category: 'test', expiredate: 'test', tax: 'test', vendor: 'test', status: 'test', isservice: 'test', shop: 'test', createdby: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.sku).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.quantity).toEqual('test')
  expect(body.unit).toEqual('test')
  expect(body.unitprice).toEqual('test')
  expect(body.sellingprice).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.expiredate).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.vendor).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.isservice).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(body.createdby).toEqual('test')
})

test('POST /items 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /items 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /items 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /items/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${items.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(items.id)
})

test('GET /items/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${items.id}`)
  expect(status).toBe(401)
})

test('GET /items/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /items/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${items.id}`)
    .send({ access_token: masterKey, name: 'test', sku: 'test', desc: 'test', quantity: 'test', unit: 'test', unitprice: 'test', sellingprice: 'test', category: 'test', expiredate: 'test', tax: 'test', vendor: 'test', status: 'test', isservice: 'test', shop: 'test', createdby: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(items.id)
  expect(body.name).toEqual('test')
  expect(body.sku).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.quantity).toEqual('test')
  expect(body.unit).toEqual('test')
  expect(body.unitprice).toEqual('test')
  expect(body.sellingprice).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.expiredate).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.vendor).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.isservice).toEqual('test')
  expect(body.shop).toEqual('test')
  expect(body.createdby).toEqual('test')
})

test('PUT /items/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${items.id}`)
  expect(status).toBe(401)
})

test('PUT /items/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, name: 'test', sku: 'test', desc: 'test', quantity: 'test', unit: 'test', unitprice: 'test', sellingprice: 'test', category: 'test', expiredate: 'test', tax: 'test', vendor: 'test', status: 'test', isservice: 'test', shop: 'test', createdby: 'test' })
  expect(status).toBe(404)
})

test('DELETE /items/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${items.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /items/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${items.id}`)
  expect(status).toBe(401)
})

test('DELETE /items/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
