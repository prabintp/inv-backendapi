import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { AccountType } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, accountType

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  accountType = await AccountType.create({})
})

test('POST /accounttype 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.isdelete).toEqual('test')
})

test('POST /accounttype 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /accounttype 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /accounttype 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /accounttype 401 (user)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /accounttype 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /accounttype/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${accountType.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(accountType.id)
})

test('GET /accounttype/:id 401 (user)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${accountType.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /accounttype/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${accountType.id}`)
  expect(status).toBe(401)
})

test('GET /accounttype/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})

test('PUT /accounttype/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${accountType.id}`)
    .send({ access_token: masterKey, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(accountType.id)
  expect(body.name).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.isdelete).toEqual('test')
})

test('PUT /accounttype/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accountType.id}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('PUT /accounttype/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accountType.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /accounttype/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accountType.id}`)
  expect(status).toBe(401)
})

test('PUT /accounttype/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(404)
})

test('DELETE /accounttype/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accountType.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /accounttype/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accountType.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('DELETE /accounttype/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accountType.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /accounttype/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accountType.id}`)
  expect(status).toBe(401)
})

test('DELETE /accounttype/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
