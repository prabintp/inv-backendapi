import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token, master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export AccountType, { schema } from './model'

const router = new Router()
const { name, desc, isdelete, shop, group, createdBy } = schema.tree

/**
 * @api {post} /accounttype Create account type
 * @apiName CreateAccountType
 * @apiGroup AccountType
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Account type's name.
 * @apiParam desc Account type's desc.
 * @apiParam isdelete Account type's isdelete.
 * @apiParam shop Account type shop.
 * @apiParam createdBy Account type created user.
 * @apiSuccess {Object} accountType Account type's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account type not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true }),
  body({ name, desc, isdelete, shop, group, createdBy }),
  create)

/**
 * @api {get} /accounttype Retrieve account types
 * @apiName RetrieveAccountTypes
 * @apiGroup AccountType
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of account types.
 * @apiSuccess {Object[]} rows List of account types.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop: {
    paths: ['shop']
   }
  }),index)

 

/**
 * @api {get} /accounttype/:id Retrieve account type
 * @apiName RetrieveAccountType
 * @apiGroup AccountType
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} accountType Account type's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account type not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true, roles: ['admin'] }),
  show)

/**
 * @api {put} /accounttype/:id Update account type
 * @apiName UpdateAccountType
 * @apiGroup AccountType
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Account type's name.
 * @apiParam desc Account type's desc.
 * @apiParam isdelete Account type's isdelete.
 * @apiSuccess {Object} accountType Account type's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account type not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin'] }),
  body({ name, desc, isdelete }),
  update)

/**
 * @api {delete} /accounttype/:id Delete account type
 * @apiName DeleteAccountType
 * @apiGroup AccountType
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Account type not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
