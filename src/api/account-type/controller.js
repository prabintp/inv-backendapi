import { success, notFound } from '../../services/response/'
import { AccountType } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  AccountType.create(body)
    .then((accountType) => accountType.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  AccountType.count(query)
    .then(count => AccountType.find(query, select, cursor)
      .then((accountTypes) => ({
        count,
        rows: accountTypes.map((accountType) => accountType.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  AccountType.findById(params.id)
    .then(notFound(res))
    .then((accountType) => accountType ? accountType.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  AccountType.findById(params.id)
    .then(notFound(res))
    .then((accountType) => accountType ? Object.assign(accountType, body).save() : null)
    .then((accountType) => accountType ? accountType.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  AccountType.findById(params.id)
    .then(notFound(res))
    .then((accountType) => accountType ? accountType.remove() : null)
    .then(success(res, 204))
    .catch(next)
