import { AccountType } from '.'

let accountType

beforeEach(async () => {
  accountType = await AccountType.create({ name: 'test', desc: 'test', isdelete: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = accountType.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(accountType.id)
    expect(view.name).toBe(accountType.name)
    expect(view.desc).toBe(accountType.desc)
    expect(view.isdelete).toBe(accountType.isdelete)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = accountType.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(accountType.id)
    expect(view.name).toBe(accountType.name)
    expect(view.desc).toBe(accountType.desc)
    expect(view.isdelete).toBe(accountType.isdelete)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
