import mongoose, { Schema } from 'mongoose'

const accountTypeSchema = new Schema({
  name: {
    type: String
  },
  desc: {
    type: String
  },
  group: {
    type: String
  },
  group_formatted: {
    type: String
  },
  name_formatted: {
    type: String
  },
  can_show_opening_balance: {
    type: Boolean
  },
  isdelete: {
    type: String
  },
  shop: {
    type: Schema.ObjectId,
    ref: 'Shop',
    required: true
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

accountTypeSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      desc: this.desc,
      isdelete: this.isdelete
    }

    return full ? {
      ...view,
      shop: this.shop.view(),
      createdBy: this.createdBy.view(),
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('AccountType', accountTypeSchema)

export const schema = model.schema
export default model
