import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Logs, { schema } from './model'

const router = new Router()
const { action, category, createdBy, message, diff, shop, itemId, value } = schema.tree

/**
 * @api {post} /logs Create logs
 * @apiName CreateLogs
 * @apiGroup Logs
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam action Logs's action.
 * @apiParam category Logs's category.
 * @apiParam createdBy Logs's createdBy.
 * @apiParam message Logs's message.
 * @apiParam diff Logs's diff.
 * @apiParam shop Logs's shop.
 * @apiSuccess {Object} logs Logs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Logs not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  body({ action, category, createdBy, message, diff, shop, itemId, value }),
  create)

/**
 * @api {get} /logs Retrieve logs
 * @apiName RetrieveLogs
 * @apiGroup Logs
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} logs List of logs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
token({ required: true, roles: ['admin', 'superAdmin'] }),
  query({
    shop: {
      paths: ['shop']
    }
  }),
  index)

/**
 * @api {get} /logs/:id Retrieve logs
 * @apiName RetrieveLogs
 * @apiGroup Logs
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} logs Logs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Logs not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /logs/:id Update logs
 * @apiName UpdateLogs
 * @apiGroup Logs
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam action Logs's action.
 * @apiParam category Logs's category.
 * @apiParam createdBy Logs's createdBy.
 * @apiParam message Logs's message.
 * @apiParam diff Logs's diff.
 * @apiParam shop Logs's shop.
 * @apiSuccess {Object} logs Logs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Logs not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ action, category, createdBy, message, diff, shop, itemId, value }),
  update)

/**
 * @api {delete} /logs/:id Delete logs
 * @apiName DeleteLogs
 * @apiGroup Logs
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Logs not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
