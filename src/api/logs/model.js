import mongoose, { Schema } from 'mongoose'


const logsSchema = new Schema({
  action: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  message: {
    type: String
  },
  diff: {
    type: Schema.Types.Mixed 
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop',
  },
  itemId: {
    type: String
  },
  itemName: {
    type: String
  },
  value: {
    type: String
  },
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

logsSchema.index({ action: 1, category: 1 })

logsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      action: this.action,
      category: this.category,
      createdBy: this.createdBy,
      message: this.message,
      itemId: this.itemId,
      itemName: this.itemName,
      shop: this.shop,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      value: this.value,

    }

    return full ? {
      ...view,
      diff: this.diff,
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Logs', logsSchema)

export const schema = model.schema
export default model
