import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Logs } from '.'

const app = () => express(apiRoot, routes)

let logs

beforeEach(async () => {
  logs = await Logs.create({})
})

test('POST /logs 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, action: 'test', category: 'test', createdBy: 'test', message: 'test', diff: 'test', shop: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.action).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.createdBy).toEqual('test')
  expect(body.message).toEqual('test')
  expect(body.diff).toEqual('test')
  expect(body.shop).toEqual('test')
})

test('POST /logs 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /logs 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /logs 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /logs/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${logs.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(logs.id)
})

test('GET /logs/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${logs.id}`)
  expect(status).toBe(401)
})

test('GET /logs/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /logs/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${logs.id}`)
    .send({ access_token: masterKey, action: 'test', category: 'test', createdBy: 'test', message: 'test', diff: 'test', shop: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(logs.id)
  expect(body.action).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.createdBy).toEqual('test')
  expect(body.message).toEqual('test')
  expect(body.diff).toEqual('test')
  expect(body.shop).toEqual('test')
})

test('PUT /logs/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${logs.id}`)
  expect(status).toBe(401)
})

test('PUT /logs/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, action: 'test', category: 'test', createdBy: 'test', message: 'test', diff: 'test', shop: 'test' })
  expect(status).toBe(404)
})

test('DELETE /logs/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${logs.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /logs/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${logs.id}`)
  expect(status).toBe(401)
})

test('DELETE /logs/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
