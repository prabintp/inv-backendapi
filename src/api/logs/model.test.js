import { Logs } from '.'

let logs

beforeEach(async () => {
  logs = await Logs.create({ action: 'test', category: 'test', createdBy: 'test', message: 'test', diff: 'test', shop: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = logs.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(logs.id)
    expect(view.action).toBe(logs.action)
    expect(view.category).toBe(logs.category)
    expect(view.createdBy).toBe(logs.createdBy)
    expect(view.message).toBe(logs.message)
    expect(view.diff).toBe(logs.diff)
    expect(view.shop).toBe(logs.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = logs.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(logs.id)
    expect(view.action).toBe(logs.action)
    expect(view.category).toBe(logs.category)
    expect(view.createdBy).toBe(logs.createdBy)
    expect(view.message).toBe(logs.message)
    expect(view.diff).toBe(logs.diff)
    expect(view.shop).toBe(logs.shop)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
