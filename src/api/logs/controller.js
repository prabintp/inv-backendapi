import { success, notFound } from '../../services/response/'
import { Logs } from '.'
import { query } from 'express'

export const create = ({ bodymen: { body } }, res, next) =>
  Logs.create(body)
    .then((logs) => logs.view(false))
    .then(success(res, 201))
    .catch(next)



    export const index = ({ querymen: { query, select, cursor } }, res, next) =>
    Logs.count(query)
      .then(count => Logs.find(query, select, cursor)
      .populate({ path: 'createdBy', select: 'name _id picture email' })
      .populate({ path: 'shop', select: '_id name' })
        .then(logs => ({
          rows: logs.map((logs) => logs.view(true)),
          count
        }))
      )
      .then(success(res))
      .catch(next)

export const show = ({ params }, res, next) =>
  Logs.findById(params.id)
    .then(notFound(res))
    .then((logs) => logs ? logs.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Logs.findById(params.id)
    .then(notFound(res))
    .then((logs) => logs ? Object.assign(logs, body).save() : null)
    .then((logs) => logs ? logs.view() : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Logs.findById(params.id)
    .then(notFound(res))
    .then((logs) => logs ? logs.remove() : null)
    .then(success(res, 204))
    .catch(next)
