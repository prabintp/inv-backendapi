import { success, notFound, authorOrAdmin } from '../../services/response/'
import mongoose from 'mongoose'
import { Invoice } from '.'
import { Transaction } from '../transaction'

// not using legacy need to confirm
export const create1 = ({ user, bodymen: { body } }, res, next) =>
  Invoice.create({ ...body, user })
    .then((invoice) => invoice.view(true))
    .then(success(res, 201))
    .catch(next)


export const create = (req, res, next) =>
  Invoice.create(req.body)
    .then((invoice) => {
      var inv = invoice.view(true);
      res.status(201).json(inv);
      return invoice;
    })
    .then(invoice => {
      if (invoice && typeof invoice.log === 'function') {
        const data = {
          action: 'create',
          category: invoice.doc_type,
          createdBy: invoice.createdBy,
          shop: invoice.shop,
          itemId: invoice._id,
          itemName: invoice.invoice_number,
          message: `Invoice created`,
        }
        return invoice.log(data)
      }
    }).catch(err => {
      console.log('Caught error while logging: ', err)
    })

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Invoice.count(query)
    .then(count => Invoice.find(query, select, cursor)
      .populate('user')
      .then((invoices) => ({
        count,
        rows: invoices.map((invoice) => invoice.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const getsale = ({ querymen: { query, select, cursor } }, res, next) =>
  Invoice.count(query)
    .then(count => Invoice.find(query, select, cursor)
      .populate('user')
      .then((invoices) => ({
        count,
        rows: invoices.map((invoice) => invoice.view())
      }))
    )
    .then(success(res))
    .catch(next)


export const show = ({ params }, res, next) =>
  Invoice.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((invoice) => invoice ? invoice.view() : null)
    .then(success(res))
    .catch(next)

export const showView = ({ params }, res, next) =>
  Invoice.findById(params.id)
    .populate('user')
    .populate({ path: 'customer', select: 'name id baddress company saddress email trn' })
    .populate({ path: 'shop', select: 'name id address print trn bank email' })
    .populate({ path: 'line_items.item_tax', select: 'name id' })
    .then(notFound(res))
    .then((invoice) => invoice ? invoice.view(true) : null)
    .then(success(res))
    .catch(next)


export const update1 = ({ user, bodymen: { body }, params }, res, next) =>
  Invoice.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((invoice) => invoice ? Object.assign(invoice, body).save() : null)
    .then((invoice) => invoice ? invoice.view(true) : null)
    .then(success(res))
    .catch(next)

export const update = (req, res, next) =>
  Invoice.findById(req.params.id)
    //  .populate('user')
    .then(notFound(res))
    //  .then(authorOrAdmin(res, user, 'user'))
    .then((invoice) => {
      return invoice ? Object.assign(invoice, req.body).save() : null;
    })
    .then((invoice) => {
      var inv = invoice ? invoice.view(true) : null;
      res.json(inv);
      return invoice;
    })
    .then(invoice => {
      if (invoice && typeof invoice.log === 'function') {
        const data = {
          action: 'update',
          category: invoice.doc_type,
          createdBy: invoice.updatedBy,
          shop: invoice.shop,
          itemId: invoice._id,
          itemName: invoice.invoice_number,
          message: `Invoice updated`,
        }
        return invoice.log(data)
      }
    }).catch(err => {
      console.log('Caught error while logging: ', err)
    })

export const destroy = ({ user, params }, res, next) =>
  Invoice.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((invoice) => invoice ? invoice.remove() : null)
    .then(success(res, 204))
    .catch(next)



export const checkuniqe = (req, res, next) =>
  Invoice.findOne({ invoice_number: req.body.invoice_number, shop: req.body.shop }, 'invoice_number shop')
    .then(notFound(res))
    .then(success(res))
    .catch(next)

export const sales = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log(cursor);
  query.shop = mongoose.Types.ObjectId(query.shop);
  if (query.customer) {
    query.customer = mongoose.Types.ObjectId(query.customer);
  }
  if (query._id) {
    query._id = mongoose.Types.ObjectId(query._id);
  }
  Invoice.aggregate([
    { "$match": query },
    { $sort: cursor.sort },
    { $limit: cursor.limit },
    {
      $lookup: {
        from: Transaction.collection.name,
        let: { invid: "$_id" },

        pipeline: [
          { $addFields: { 'amt': {$round: [{ $divide: ['$amount', 100] }, 3]} } },
          { "$match": { "$expr": { "$eq": ["$invoice", "$$invid"] } } }
        ],
        // localField: '_id',
        // foreignField: 'invoice',
        as: 'payments'
      }
    },
    { $addFields: { 'id': '$_id', balance: { $subtract: ['$total', { $sum: ['$payments.amt'] }] } } }

  ]).then(notFound(res))
    .then((invoices) => ({
      count: invoices.length,
      rows: invoices
    }))
    .then(success(res))
    .catch(next)
}

//Receivables section displays how much money your customers owe you. This section is divided into two parts:


export const receivables = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log(cursor);
  query.shop = mongoose.Types.ObjectId(query.shop);
  if (query.customer) {
    query.customer = mongoose.Types.ObjectId(query.customer);
  }
  if (query._id) {
    query._id = mongoose.Types.ObjectId(query._id);
  }
  Invoice.aggregate([
    { "$match": query },
    { $sort: cursor.sort },
    { $limit: cursor.limit },
    {
      $lookup: {
        from: Transaction.collection.name,
        let: { invid: "$_id" },

        pipeline: [
          { $addFields: { 'amt': { $divide: ['$amount', 100] } } },
          { "$match": { "$expr": { "$eq": ["$invoice", "$$invid"] } } }
        ],
        // localField: '_id',
        // foreignField: 'invoice',
        as: 'payments'
      }
    },
    {
      $addFields: {
        'id': '$_id',
        balance: {
          $subtract: ['$total',
            { $divide: [{ $sum: ['$payments.amount'] }, 100] }
          ]
        },






        //Overdue: The amount you are yet to receive for invoices that have crossed the due date.
        balance_due: {
          $cond: [{
            $lt: [{
              $dateFromParts: {
                'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
              }
            }, new Date()]
          }, {
            $subtract: ['$total',
              { $divide: [{ $sum: ['$payments.amount'] }, 100] }
            ]
          }, 0]
        },

        balance_due15: {
          $cond: [
            {
              $and: [
                {
                  $gte: [new Date(), {
                    $dateFromParts: {
                      'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                    }
                  }]
                },
                {
                  $lte: [new Date(), {
                    $add: [{
                      $dateFromParts: {
                        'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                      }
                    }, 15 * 24 * 60 * 60000]
                  }]
                }
              ]
            },
            {
              $subtract: ['$total',
                { $divide: [{ $sum: ['$payments.amount'] }, 100] }
              ]
            }, 0]
        },


        balance_due30: {
          $cond: [
            {
              $and: [
                {
                  $gt: [new Date(), {
                    $add: [{
                      $dateFromParts: {
                        'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                      }
                    }, 15 * 24 * 60 * 60000]
                  }]
                },
                {
                  $lte: [new Date(), {
                    $add: [{
                      $dateFromParts: {
                        'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                      }
                    }, 30 * 24 * 60 * 60000]
                  }]
                }
              ]
            },
            {
              $subtract: ['$total',
                { $divide: [{ $sum: ['$payments.amount'] }, 100] }
              ]
            }, 0]
        },

        balance_due45: {
          $cond: [
            {
              $and: [
                {
                  $gt: [new Date(), {
                    $add: [{
                      $dateFromParts: {
                        'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                      }
                    }, 30 * 24 * 60 * 60000]
                  }]
                },
                {
                  $lte: [new Date(), {
                    $add: [{
                      $dateFromParts: {
                        'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                      }
                    }, 45 * 24 * 60 * 60000]
                  }]
                }
              ]
            },
            {
              $subtract: ['$total',
                { $divide: [{ $sum: ['$payments.amount'] }, 100] }
              ]
            }, 0]
        },


        balance_due_above45: {
          $cond: [

            {
              $gt: [new Date(), {
                $add: [{
                  $dateFromParts: {
                    'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
                  }
                }, 45 * 24 * 60 * 60000]
              }]
            },
            {
              $subtract: ['$total',
                { $divide: [{ $sum: ['$payments.amount'] }, 100] }
              ]
            }, 0]
        },




        // Current: The amount that you are yet to receive for invoices that have not crossed the payment due date.
        balance_current: {
          $cond: [{
            $lt: [{
              $dateFromParts: {
                'year': '$invoice_due.year', 'month': '$invoice_due.month', 'day': '$invoice_due.day',
              }
            }, new Date()]
          }, 0, {
            $subtract: ['$total',
              { $divide: [{ $sum: ['$payments.amount'] }, 100] }
            ]
          }]
        },


      }
    },

    {
      $group: {
        _id: null,
        current_receivable: { $sum: '$balance_current' },
        overdue_receivable: { $sum: '$balance_due' },
        overdue_receivable15: { $sum: '$balance_due15' },
        overdue_receivable30: { $sum: '$balance_due30' },
        overdue_receivable45: { $sum: '$balance_due45' },
        overdue_receivable_above45: { $sum: '$balance_due_above45' },
        total_receivable: { $sum: '$balance' },
        //total: {$sum: '$total'}
      }
    }

  ]).then(notFound(res))
    .then((invoices) => ({
      count: invoices.length,
      rows: invoices
    }))
    .then(success(res))
    .catch(next)
}



export const salesReport = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log(cursor);
  query.shop = mongoose.Types.ObjectId(query.shop);
  if (query.customer) {
    query.customer = mongoose.Types.ObjectId(query.customer);
  }
  if (query._id) {
    query._id = mongoose.Types.ObjectId(query._id);
  }
  Invoice.aggregate([
    { "$match": query },
    { $sort: cursor.sort },
    { $limit: cursor.limit },
    /*  {
        $lookup:{
        from: Transaction.collection.name,
        let: { invid: "$_id" },
       
        pipeline: [
          { $addFields: {'amt': { $divide: ['$amount', 100 ]}}},
       { "$match": { "$expr": { "$eq": [ "$invoice", "$$invid" ] } } }
        ],
       // localField: '_id',
       // foreignField: 'invoice',
        as: 'payments'
        }
      },*/



    {
      $group: {
        // _id: "$shop",
        _id: {
          // customer: "$customer",
          // invoice_date: { $month: "$invoice_date" },
          //  invoice_date: { $dayOfMonth: "$invoice_date" },
          month: "$invoice_date.month",
          year: "$invoice_date.year",



        },
        // totalReceipts:  {$sum: '$payments.amt'},
        totalsales: { $sum: '$total' }
        // totalreceipts: {$sum: '$payments.amt'}

      }
    },
    {
      // sort descending (latest subscriptions first) 
      $sort: {
        '_id.year': -1,
        '_id.month': -1
      }
    },

  ]).then(notFound(res))
    .then((invoices) => ({
      count: invoices.length,
      rows: invoices
    }))
    .then(success(res))
    .catch(next)
}

export const receiptsReport = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log(cursor);
  query.shop = mongoose.Types.ObjectId(query.shop);
  if (query.customer) {
    query.customer = mongoose.Types.ObjectId(query.customer);
  }
  if (query._id) {
    query._id = mongoose.Types.ObjectId(query._id);
  }
  Invoice.aggregate([
    { "$match": query },
    { $sort: cursor.sort },
    { $limit: cursor.limit },
    {
      $lookup: {
        from: Transaction.collection.name,
        let: { invid: "$_id" },

        pipeline: [
          { $addFields: { 'amt': { $divide: ['$amount', 100] } } },
          { "$match": { "$expr": { "$eq": ["$invoice", "$$invid"] } } }
        ],
        // localField: '_id',
        // foreignField: 'invoice',
        as: 'payments'
      }
    },

    { $unwind: "$payments" },

    {
      $group: {
        // _id: "$shop",
        _id: {
          // customer: "$customer",
          //  invoice_date: { $month: "$invoice_date" },
          //  invoice_date: { $dayOfMonth: "$invoice_date" },
          // month: "$invoice_date.month",
          //  year: "$invoice_date.year",
          month: "$payments.txn_date.month",
          year: "$payments.txn_date.year",

        },
        totalReceipts: { $sum: '$payments.amt' },
        // totalsales: {$sum: '$total'}
        // totalreceipts: {$sum: '$payments.amt'}

      }
    },
    {
      // sort descending (latest subscriptions first) 
      $sort: {
        '_id.year': -1,
        '_id.month': -1
      }
    },

  ]).then(notFound(res))
    .then((invoices) => ({
      count: invoices.length,
      rows: invoices
    }))
    .then(success(res))
    .catch(next)
}


