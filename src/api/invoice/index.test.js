import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Invoice } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, invoice

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  invoice = await Invoice.create({ user })
})

test('POST /invoices 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, invoice_number: 'test', invoice_date: 'test', invoice_due: 'test', client: 'test', status: 'test', line_items: 'test', subtotal: 'test', total: 'test', discount: 'test', tax: 'test', notes: 'test', shipping_charge: 'test', advance_payment: 'test', desc: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.invoice_number).toEqual('test')
  expect(body.invoice_date).toEqual('test')
  expect(body.invoice_due).toEqual('test')
  expect(body.client).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.line_items).toEqual('test')
  expect(body.subtotal).toEqual('test')
  expect(body.total).toEqual('test')
  expect(body.discount).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.notes).toEqual('test')
  expect(body.shipping_charge).toEqual('test')
  expect(body.advance_payment).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /invoices 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /invoices 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
  expect(typeof body.rows[0].user).toEqual('object')
})

test('GET /invoices 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /invoices/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${invoice.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(invoice.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /invoices/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${invoice.id}`)
  expect(status).toBe(401)
})

test('GET /invoices/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /invoices/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${invoice.id}`)
    .send({ access_token: userSession, invoice_number: 'test', invoice_date: 'test', invoice_due: 'test', client: 'test', status: 'test', line_items: 'test', subtotal: 'test', total: 'test', discount: 'test', tax: 'test', notes: 'test', shipping_charge: 'test', advance_payment: 'test', desc: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(invoice.id)
  expect(body.invoice_number).toEqual('test')
  expect(body.invoice_date).toEqual('test')
  expect(body.invoice_due).toEqual('test')
  expect(body.client).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.line_items).toEqual('test')
  expect(body.subtotal).toEqual('test')
  expect(body.total).toEqual('test')
  expect(body.discount).toEqual('test')
  expect(body.tax).toEqual('test')
  expect(body.notes).toEqual('test')
  expect(body.shipping_charge).toEqual('test')
  expect(body.advance_payment).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /invoices/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${invoice.id}`)
    .send({ access_token: anotherSession, invoice_number: 'test', invoice_date: 'test', invoice_due: 'test', client: 'test', status: 'test', line_items: 'test', subtotal: 'test', total: 'test', discount: 'test', tax: 'test', notes: 'test', shipping_charge: 'test', advance_payment: 'test', desc: 'test' })
  expect(status).toBe(401)
})

test('PUT /invoices/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${invoice.id}`)
  expect(status).toBe(401)
})

test('PUT /invoices/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, invoice_number: 'test', invoice_date: 'test', invoice_due: 'test', client: 'test', status: 'test', line_items: 'test', subtotal: 'test', total: 'test', discount: 'test', tax: 'test', notes: 'test', shipping_charge: 'test', advance_payment: 'test', desc: 'test' })
  expect(status).toBe(404)
})

test('DELETE /invoices/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${invoice.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /invoices/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${invoice.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /invoices/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${invoice.id}`)
  expect(status).toBe(401)
})

test('DELETE /invoices/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})
