import mongoose, { Schema } from 'mongoose'
const stat = ['pending', 'completed']
const docType = ['invoices', 'purchaseorders','salesorders', 'quotation', 'bills']
mongoose.plugin(require('../..//utils/diff-plugin'));

const invoiceSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop',
    required: true
  },
  invoice_number: {
    type: String
  },
  invoice_date: { year: Number, month: Number, day: Number },
  invoice_due:  { year: Number, month: Number, day: Number },
  shipping_date: { year: Number, month: Number, day: Number },
   shipping_method: {
    type: String
  },
   payment_terms: {
    type: String
  },
  client: {
    type: String
  },
  doc_type: {
    type: String,
    enum: docType,
    default: 'invoices'
  },
  status: {
    type: String,
    enum: stat,
    default: 'pending'
  },
  line_items:
     [{
      item_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Items'
      },
      item_name:{
        type: String
      },
      quantity:{
        type: Number
      },
      rate:{
        type: String
      },
      item_tax: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tax'
      },
      tax_total:{
        type: String
      },
      item_total:{
          type: String
      },
      discount:{
          type: String
      },
      sku:{
        type: String
      },
      unit:{
        type: String
      },
      uom_id:{
        type: String
      }
    }],
  subtotal: {
    type: String
  },
  total: {
    type: Number,
    required: true
  },
  discount: {
    type: String
  },
  tax: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tax'
  },
  totalTax: {
    type: Number
  },
  tax_type: {
    type: String
  },
  tax_rate: {
    type: Number
  },
  notes: {
    type: String
  },
  shipping_charge: {
    type: String
  },
  advance_payment: {
    type: String
  },
  desc: {
    type: String
  },
  sales_person_name: {
    type: String
  },
  sales_person: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contacts'
  },
  purchase_order_name: {
    type: String
  },
  purchase_order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Invoice'
  },
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contacts'
  },
  customer_name: {
    type: String
  },
  delivery_time: {
    type: String
  },
  rfq_no:{
    type: String
  },
  createdBy: {
		type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
		type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
	},
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

invoiceSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user,
      shop: this.shop.view(full),
      doc_type: this.doc_type,
      invoice_number: this.invoice_number,
      invoice_date: this.invoice_date,
      invoice_due: this.invoice_due,
	  shipping_date: this.shipping_date,
	  shipping_method: this.shipping_method,
	  payment_terms: this.payment_terms,
      client: this.client,
      status: this.status,
      line_items: this.line_items,
      subtotal: this.subtotal,
      total: this.total,
      discount: this.discount,
      tax: this.tax,
      tax_rate: this.tax_rate,
      tax_type: this.tax_type,
      notes: this.notes,
      shipping_charge: this.shipping_charge,
      advance_payment: this.advance_payment,
      desc: this.desc,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      customer: this.customer,
      sales_person: this.sales_person,
	  purchase_order: this.purchase_order,
	  purchase_order_name: this.purchase_order_name,
      customer_name: this.customer_name,
      sales_person_name: this.sales_person_name,
      totalTax: this.totalTax,
      rfq_no: this.rfq_no,
      delivery_time: this.delivery_time
    }

    return full ? {
      ...view,
      // add properties for a full view
    } : view
  }
}


const model = mongoose.model('Invoice', invoiceSchema)

export const schema = model.schema
export default model
