import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import mongoose from 'mongoose'
import { create,create1, index, show, update, destroy, showView, getsale, checkuniqe, sales, receivables, salesReport, receiptsReport } from './controller'
import { schema } from './model'
export Invoice, { schema } from './model'

const router = new Router()
const { shop, invoice_number, doc_type, invoice_date, invoice_due,
 client, status, line_items, subtotal, total, discount, tax, totalTax,
 tax_rate, tax_type,customer,sales_person, customer_name,sales_person_name,
 purchase_order, purchase_order_name,  notes,
 shipping_method, shipping_date, payment_terms,
 shipping_charge, advance_payment, desc, rfq_no, _id, delivery_time } = schema.tree

/**
 * @api {post} /invoices Create invoice
 * @apiName CreateInvoice
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam invoice_number Invoice's invoice_number.
 * @apiParam invoice_date Invoice's invoice_date.
 * @apiParam invoice_due Invoice's invoice_due.
 * @apiParam client Invoice's client.
 * @apiParam status Invoice's status.
 * @apiParam line_items Invoice's line_items.
 * @apiParam subtotal Invoice's subtotal.
 * @apiParam total Invoice's total.
 * @apiParam discount Invoice's discount.
 * @apiParam tax Invoice's tax.
 * @apiParam notes Invoice's notes.
 * @apiParam shipping_charge Invoice's shipping_charge.
 * @apiParam advance_payment Invoice's advance_payment.
 * @apiParam desc Invoice's desc.
 * @apiSuccess {Object} invoice Invoice's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Invoice not found.
 * @apiError 401 user access only.
 */
router.post('/cr',
  token({ required: true }),
  body({ shop, invoice_number, tax_rate, doc_type, tax_type, invoice_date,
  invoice_due, client, status, line_items, subtotal, total, discount, tax,
  totalTax, notes,customer,sales_person,customer_name,sales_person_name,
   shipping_method, shipping_date, payment_terms,
  purchase_order, purchase_order_name, shipping_charge, advance_payment, desc, rfq_no, delivery_time }),
  create1)

  router.post('/',
    create)

/**
 * @api {get} /invoices Retrieve invoices
 * @apiName RetrieveInvoices
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of invoices.
 * @apiSuccess {Object[]} rows List of invoices.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */

 /**
 * @api {getsale} /invoices Retrieve invoices/po/so
 * @apiName RetrieveInvoices/po/so
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of invoices.
 * @apiSuccess {Object[]} rows List of invoices.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop, doc_type, invoice_number, customer}),
  index)

  router.get('/sales',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop,doc_type, invoice_number, customer, _id}),
  sales)

  router.get('/receivables',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop,doc_type, invoice_number, customer}),
    receivables)

  router.get('/salesreport',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop,doc_type, invoice_number, customer}),
    salesReport)


    router.get('/receiptsreport',
  token({ required: true }),
  query({
    limit: { max: 2000, default:2000 },
    shop,doc_type, invoice_number, customer}),
    receiptsReport)



  router.post('/getsale',
  token({ required: true }),    
  getsale)

/**
 * @api {get} /invoices/:id Retrieve invoice
 * @apiName RetrieveInvoice
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} invoice Invoice's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Invoice not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

  /**
   * @api {get} /invoices/:id Retrieve invoice
   * @apiName RetrieveInvoice
   * @apiGroup Invoice
   * @apiPermission user
   * @apiParam {String} access_token user access token.
   * @apiSuccess {Object} invoice Invoice's data.
   * @apiError {Object} 400 Some parameters may contain invalid values.
   * @apiError 404 Invoice not found.
   * @apiError 401 user access only.
   */
  router.get('/:id/view',
    token({ required: true }),
    showView)

/**
 * @api {put} /invoices/:id Update invoice
 * @apiName UpdateInvoice
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam invoice_number Invoice's invoice_number.
 * @apiParam invoice_date Invoice's invoice_date.
 * @apiParam invoice_due Invoice's invoice_due.
 * @apiParam client Invoice's client.
 * @apiParam status Invoice's status.
 * @apiParam line_items Invoice's line_items.
 * @apiParam subtotal Invoice's subtotal.
 * @apiParam total Invoice's total.
 * @apiParam discount Invoice's discount.
 * @apiParam tax Invoice's tax.
 * @apiParam notes Invoice's notes.
 * @apiParam shipping_charge Invoice's shipping_charge.
 * @apiParam advance_payment Invoice's advance_payment.
 * @apiParam desc Invoice's desc.
 * @apiSuccess {Object} invoice Invoice's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Invoice not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
//  token({ required: true })
//  body({ invoice_number, invoice_date, invoice_due, client, status, line_items, subtotal, total, discount, tax, notes, shipping_charge, advance_payment, desc }),
  update)

/**
 * @api {delete} /invoices/:id Delete invoice
 * @apiName DeleteInvoice
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Invoice not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

  /**
 * @api {getsale} /invoices Retrieve invoices/po/so
 * @apiName RetrieveInvoices/po/so
 * @apiGroup Invoice
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of invoices.
 * @apiSuccess {Object[]} rows List of invoices.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
/* router.get('/isuniqeid',
token({ required: true }),
checkuniqe) */

router.post('/isuniqe',
  token({ required: true }),
// query({invoice_number, shop}),
 // select({invoice_number}),
  checkuniqe)

export default router


/*
No Title
apiRoutes.post(`/${SERVICE_CONST.GET_MY_POSTS}`, function (req, res) {
        let reqdata = req.body, postid = '', obj = {};
        if (req.body.hasOwnProperty('postid')) {
            postid = req.body.postid;
        }


        if (reqdata.userid !== '') {
            obj = {'_author': mongoose.Types.ObjectId(cryptr.decrypt(reqdata.userid))};
            if (postid !== '') {
                obj._id = mongoose.Types.ObjectId(postid);
            }
        } else {
            obj.flag = 'p';
        }

        Posts.aggregate([
            {"$match": obj},
            {$sort: {date: -1}},
            {$lookup: {from: 'users', localField: '_author', foreignField: '_id', as: 'userDetail'}}, // post+ user
            {$lookup: {from: 'comments', localField: '_id', foreignField: 'postid', as: 'commentdata'}}, // post+ comments
            // { $lookup: { from: 'commentdata', foreignField: 'commentdata.postby', localField:'users._id', as:'commentuser'}},
            {$project: {"userDetail": {"registerTime": 0, "friends": 0, "_id": 0, "token": 0, "city": 0, "password": 0, "userid": 0}}}


        ]).exec((error, results) => {
            if (error) {
                res.json({status: error});
            }

            var contr = new UserController();
            contr.getPostDetails(results, req.body.onlytext);
            res.json({status: "Post Listing", posts: results, obj: obj});
        });
    });


*/
