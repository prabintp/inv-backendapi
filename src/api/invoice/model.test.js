import { Invoice } from '.'
import { User } from '../user'

let user, invoice

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  invoice = await Invoice.create({ user, invoice_number: 'test', invoice_date: 'test', invoice_due: 'test', client: 'test', status: 'test', line_items: 'test', subtotal: 'test', total: 'test', discount: 'test', tax: 'test', notes: 'test', shipping_charge: 'test', advance_payment: 'test', desc: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = invoice.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(invoice.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.invoice_number).toBe(invoice.invoice_number)
    expect(view.invoice_date).toBe(invoice.invoice_date)
    expect(view.invoice_due).toBe(invoice.invoice_due)
    expect(view.client).toBe(invoice.client)
    expect(view.status).toBe(invoice.status)
    expect(view.line_items).toBe(invoice.line_items)
    expect(view.subtotal).toBe(invoice.subtotal)
    expect(view.total).toBe(invoice.total)
    expect(view.discount).toBe(invoice.discount)
    expect(view.tax).toBe(invoice.tax)
    expect(view.notes).toBe(invoice.notes)
    expect(view.shipping_charge).toBe(invoice.shipping_charge)
    expect(view.advance_payment).toBe(invoice.advance_payment)
    expect(view.desc).toBe(invoice.desc)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = invoice.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(invoice.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.invoice_number).toBe(invoice.invoice_number)
    expect(view.invoice_date).toBe(invoice.invoice_date)
    expect(view.invoice_due).toBe(invoice.invoice_due)
    expect(view.client).toBe(invoice.client)
    expect(view.status).toBe(invoice.status)
    expect(view.line_items).toBe(invoice.line_items)
    expect(view.subtotal).toBe(invoice.subtotal)
    expect(view.total).toBe(invoice.total)
    expect(view.discount).toBe(invoice.discount)
    expect(view.tax).toBe(invoice.tax)
    expect(view.notes).toBe(invoice.notes)
    expect(view.shipping_charge).toBe(invoice.shipping_charge)
    expect(view.advance_payment).toBe(invoice.advance_payment)
    expect(view.desc).toBe(invoice.desc)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
