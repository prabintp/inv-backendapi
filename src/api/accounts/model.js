import mongoose, { Schema } from 'mongoose'

const accountsSchema = new Schema({
  name: {
    type: String
  },
  desc: {
    type: String
  },
  isdelete: {
    type: String
  },
  isactive: {
    type: Boolean,
    default: true
  },
  is_system_account: {
    type: Boolean,
    default: false
  },
  shop: {
    type: Schema.ObjectId,
    ref: 'Shop',
    required: true
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  accounttype: {
    type: Schema.ObjectId,
    ref: 'AccountType',
  },
  account_type: {
    type: String
  },
  account_type_formatted: {
    type: String
  },
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

accountsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      desc: this.desc,
      isdelete: this.isdelete,
      accounttype: this.accounttype,
      account_type: this.account_type,
      account_type_formatted: this.account_type_formatted,
      shop: this.shop,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view,
      shop: this.shop,
      createdBy: this.createdBy
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Accounts', accountsSchema)

export const schema = model.schema
export default model
