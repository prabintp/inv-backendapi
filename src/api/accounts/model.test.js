import { Accounts } from '.'

let accounts

beforeEach(async () => {
  accounts = await Accounts.create({ name: 'test', desc: 'test', isdelete: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = accounts.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(accounts.id)
    expect(view.name).toBe(accounts.name)
    expect(view.desc).toBe(accounts.desc)
    expect(view.isdelete).toBe(accounts.isdelete)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = accounts.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(accounts.id)
    expect(view.name).toBe(accounts.name)
    expect(view.desc).toBe(accounts.desc)
    expect(view.isdelete).toBe(accounts.isdelete)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
