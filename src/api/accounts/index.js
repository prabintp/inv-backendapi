import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Accounts, { schema } from './model'

const router = new Router()
const { name, desc, isdelete, shop, createdBy, accounttype, updatedAt, is_system_account, account_type, account_type_formatted } = schema.tree

/**
 * @api {post} /accounts Create accounts
 * @apiName CreateAccounts
 * @apiGroup Accounts
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Accounts's name.
 * @apiParam desc Accounts's desc.
 * @apiParam isdelete Accounts's isdelete.
 * @apiParam shop Accounts shop.
 * @apiParam createdBy Accounts created user.
 * @apiParam accounttype Accounts type.
 * @apiSuccess {Object} accounts Accounts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Accounts not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin'] }),
  body({ name, desc, isdelete, shop, createdBy, accounttype, is_system_account, account_type, account_type_formatted }),
  create)

/**
 * @api {get} /accounts Retrieve accounts
 * @apiName RetrieveAccounts
 * @apiGroup Accounts
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of accounts.
 * @apiSuccess {Object[]} rows List of accounts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true}),
  query({
    limit: { max: 2000, default:2000 },
    shop: {
    paths: ['shop']
   },
   accounttype: {
    paths: ['accounttype']
   }
  }),
  index)

/**
 * @api {get} /accounts/:id Retrieve accounts
 * @apiName RetrieveAccounts
 * @apiGroup Accounts
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} accounts Accounts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Accounts not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /accounts/:id Update accounts
 * @apiName UpdateAccounts
 * @apiGroup Accounts
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Accounts's name.
 * @apiParam desc Accounts's desc.
 * @apiParam isdelete Accounts's isdelete.
 * @apiParam shop Accounts shop.
 * @apiParam createdBy Accounts created user.
 * @apiParam accounttype Accounts type.
 * @apiSuccess {Object} accounts Accounts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Accounts not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin'] }),
  body({ name, desc, isdelete, accounttype, updatedAt }),
  update)

/**
 * @api {delete} /accounts/:id Delete accounts
 * @apiName DeleteAccounts
 * @apiGroup Accounts
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Accounts not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
