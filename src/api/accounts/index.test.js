import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Accounts } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, accounts

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  accounts = await Accounts.create({})
})

test('POST /accounts 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.isdelete).toEqual('test')
})

test('POST /accounts 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /accounts 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /accounts 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /accounts 401 (user)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /accounts 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /accounts/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${accounts.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(accounts.id)
})

test('GET /accounts/:id 401 (user)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${accounts.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /accounts/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${accounts.id}`)
  expect(status).toBe(401)
})

test('GET /accounts/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})

test('PUT /accounts/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${accounts.id}`)
    .send({ access_token: masterKey, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(accounts.id)
  expect(body.name).toEqual('test')
  expect(body.desc).toEqual('test')
  expect(body.isdelete).toEqual('test')
})

test('PUT /accounts/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accounts.id}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('PUT /accounts/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accounts.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /accounts/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${accounts.id}`)
  expect(status).toBe(401)
})

test('PUT /accounts/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, name: 'test', desc: 'test', isdelete: 'test' })
  expect(status).toBe(404)
})

test('DELETE /accounts/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accounts.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /accounts/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accounts.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('DELETE /accounts/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accounts.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /accounts/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${accounts.id}`)
  expect(status).toBe(401)
})

test('DELETE /accounts/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
