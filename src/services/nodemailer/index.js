const nodemailer = require('nodemailer');
const { google } = require('googleapis');
import { mail } from '../../config'
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';

const oAuth2Client = new google.auth.OAuth2(
    mail.clientId,
    mail.secret,
    REDIRECT_URI
  );

export const sendMail =  ({
  fromEmail = mail.defaultEmail,
  toEmail,
  subject,
  content,
  contentType = 'text/html'
}) => {

 oAuth2Client.setCredentials({
    refresh_token: mail.refreshToken,
  });
  const accessToken =  oAuth2Client.getAccessToken().catch((error) => {});;
  console.log('at',accessToken);

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        type: 'OAuth2',
        clientId: mail.client,
        clientSecret: mail.secret,
        user: 'iamprabintp@gmail.com',
        refreshToken: mail.refreshToken,
        accessToken: accessToken
       // accessToken: 'ya29.a0AfH6SMDrxKTM-KIQ9rhWY8hvNznaLmWUQwctpoF_os92-L_maYDXO9BDZU5sC_ifAT2x0P3IGfN5rI2XyB6kB3OR9v3TdAj4Jfp1XVQG55PwwLiUy86hFt8VvGSYdfZbvEPuSa097UMchTNozo9HACtVIEC2',
       // expires: 1484314697598
      }
  });
  var mailOptions = {
    from: fromEmail,
    to: toEmail,
    subject: subject,
    html: content,  
  }

  return transporter.sendMail(mailOptions);

  //return sg.API(request)
}
