const _ = require('lodash')
//const Logs = require('../api/logs/model')

import {Logs} from '../api/logs'

const { getDiff } = require('./diff')

const plugin = function (schema) {
  schema.post('init', doc => {
    doc._original = doc.toObject({transform: false})
  })
  schema.pre('save', function (next) {
    if (this.isNew) {
      next()
    }else {
      this._diff = getDiff(this, this._original)
      console.log('diff',this._diff );
      next()
    }
})

  schema.methods.log = function (data)  {
    data.diff = {
      before: this._original,
      after: this._diff,
    }
    return Logs.create(data)
  }
}

module.exports = plugin