/* eslint-disable no-unused-vars */
import path from 'path'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}
console.log('env',process.env.NODE_ENV);
/* istanbul ignore next */
if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv-safe')
  dotenv.load({
    path: path.join(__dirname, '../.env.example'),
    sample: path.join(__dirname, '../.env.example')
  })
} 

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 9090,
    ip: process.env.IP || '0.0.0.0',
    apiRoot: process.env.API_ROOT || '/api',
    defaultEmail: 'iamprabintp@gmail.com',
    sendgridKey: requireProcessEnv('SENDGRID_KEY'),
    masterKey: requireProcessEnv('MASTER_KEY'),
    jwtSecret: requireProcessEnv('JWT_SECRET'),
    mail: {
      client: requireProcessEnv('MAIL_CLIENT'),
      secret: requireProcessEnv('MAIL_SECRET'),
      refreshToken: requireProcessEnv('MAIL_RTOKEN'),
      defaultEmail: 'iamprabintp@gmail.com',
    },
    mongo: {
      options: {
        db: {
          safe: true
        }
      }
    }
  },
  local: {
    mongo: {
      uri: 'mongodb://127.0.0.1:27017/heroku_6snpfjzx',
      options: {
        debug: true
      }
    }
  },
  development: {
    mongo: {
    //  uri: 'mongodb+srv://dbuser:Passw0rd@cluster-6snpfjzx.utmon.mongodb.net/heroku_6snpfjzx?retryWrites=true&w=majority',
      uri: 'mongodb+srv://dbuser:Passw0rd@cluster0.dndczn9.mongodb.net/heroku_6snpfjzx?retryWrites=true&w=majority',
     
    //  uri: 'mongodb://127.0.0.1:27017/heroku_6snpfjzx',
      options: {
        debug: true
      }
    }
  },
  production: {
    ip: process.env.IP || undefined,
    port: process.env.PORT || 8080,
    mongo: {
      uri: process.env.MONGODB_URI || 'mongodb://localhost/prodapi',
      options: {
        debug: true
      }
    }
  }
}
console.log('config', Object.assign(config.all, config[config.all.env]));
module.exports = Object.assign(config.all, config[config.all.env])
export default module.exports
