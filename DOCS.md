# prodapi v0.0.0



- [AccountType](#accounttype)
	- [Create account type](#create-account-type)
	- [Delete account type](#delete-account-type)
	- [Retrieve account type](#retrieve-account-type)
	- [Retrieve account types](#retrieve-account-types)
	- [Update account type](#update-account-type)
	
- [Accounts](#accounts)
	- [Create accounts](#create-accounts)
	- [Delete accounts](#delete-accounts)
	- [Retrieve accounts](#retrieve-accounts)
	- [Update accounts](#update-accounts)
	
- [Auth](#auth)
	- [Authenticate](#authenticate)
	- [Authenticate with Facebook](#authenticate-with-facebook)
	- [Authenticate with Google](#authenticate-with-google)
	
- [Blog](#blog)
	- [Create blog](#create-blog)
	- [Delete blog](#delete-blog)
	- [Retrieve blog](#retrieve-blog)
	- [Retrieve blogs](#retrieve-blogs)
	- [Update blog](#update-blog)
	
- [Category](#category)
	- [Create category](#create-category)
	- [Delete category](#delete-category)
	- [Retrieve categories](#retrieve-categories)
	- [Retrieve category](#retrieve-category)
	- [Update category](#update-category)
	
- [Contacts](#contacts)
	- [Create contacts](#create-contacts)
	- [Delete contacts](#delete-contacts)
	- [Retrieve contacts](#retrieve-contacts)
	- [Update contacts](#update-contacts)
	
- [Invoice](#invoice)
	- [Create invoice](#create-invoice)
	- [Delete invoice](#delete-invoice)
	- [Retrieve invoice](#retrieve-invoice)
	- [Retrieve invoices](#retrieve-invoices)
	- [Retrieve invoices/po/so](#retrieve-invoices/po/so)
	- [Update invoice](#update-invoice)
	
- [Items](#items)
	- [Create items](#create-items)
	- [Delete items](#delete-items)
	- [Retrieve items](#retrieve-items)
	- [Update items](#update-items)
	
- [PasswordReset](#passwordreset)
	- [Send email](#send-email)
	- [Submit password](#submit-password)
	- [Verify token](#verify-token)
	
- [Shop](#shop)
	- [Create shop](#create-shop)
	- [Delete shop](#delete-shop)
	- [Retrieve shop](#retrieve-shop)
	- [Retrieve shops](#retrieve-shops)
	- [Update shop](#update-shop)
	- [Delete shop](#delete-shop)
	
- [Tax](#tax)
	- [Create tax](#create-tax)
	- [Delete tax](#delete-tax)
	- [Retrieve tax](#retrieve-tax)
	- [Retrieve taxes](#retrieve-taxes)
	- [Update tax](#update-tax)
	
- [Transaction](#transaction)
	- [Create transaction](#create-transaction)
	- [Delete transaction](#delete-transaction)
	- [Retrieve transaction](#retrieve-transaction)
	- [Retrieve transactions](#retrieve-transactions)
	- [Update transaction](#update-transaction)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# AccountType

## Create account type



	POST /accounttype


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Account type's name.</p>							|
| desc			| 			|  <p>Account type's desc.</p>							|
| isdelete			| 			|  <p>Account type's isdelete.</p>							|
| shop			| 			|  <p>Account type shop.</p>							|
| createdBy			| 			|  <p>Account type created user.</p>							|

## Delete account type



	DELETE /accounttype/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve account type



	GET /accounttype/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve account types



	GET /accounttype


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update account type



	PUT /accounttype/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Account type's name.</p>							|
| desc			| 			|  <p>Account type's desc.</p>							|
| isdelete			| 			|  <p>Account type's isdelete.</p>							|

# Accounts

## Create accounts



	POST /accounts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Accounts's name.</p>							|
| desc			| 			|  <p>Accounts's desc.</p>							|
| isdelete			| 			|  <p>Accounts's isdelete.</p>							|
| shop			| 			|  <p>Accounts shop.</p>							|
| createdBy			| 			|  <p>Accounts created user.</p>							|
| accounttype			| 			|  <p>Accounts type.</p>							|

## Delete accounts



	DELETE /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve accounts



	GET /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Update accounts



	PUT /accounts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Accounts's name.</p>							|
| desc			| 			|  <p>Accounts's desc.</p>							|
| isdelete			| 			|  <p>Accounts's isdelete.</p>							|
| shop			| 			|  <p>Accounts shop.</p>							|
| createdBy			| 			|  <p>Accounts created user.</p>							|
| accounttype			| 			|  <p>Accounts type.</p>							|

# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

## Authenticate with Facebook



	POST /auth/facebook


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Facebook user accessToken.</p>							|

## Authenticate with Google



	POST /auth/google


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Google user accessToken.</p>							|

# Blog

## Create blog



	POST /blogs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Blog's name.</p>							|
| content			| 			|  <p>Blog's content.</p>							|

## Delete blog



	DELETE /blogs/:id


## Retrieve blog



	GET /blogs/:id


## Retrieve blogs



	GET /blogs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update blog



	PUT /blogs/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Blog's name.</p>							|
| content			| 			|  <p>Blog's content.</p>							|

# Category

## Create category



	POST /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Category's name.</p>							|
| desc			| 			|  <p>Category's desc.</p>							|
| shop			| 			|  <p>Category's shop.</p>							|

## Delete category



	DELETE /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve categories



	GET /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve category



	GET /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Update category



	PUT /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Category's name.</p>							|
| desc			| 			|  <p>Category's desc.</p>							|

# Contacts

## Create contacts



	POST /contacts


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Contacts's name.</p>							|
| email			| 			|  <p>Contacts's email.</p>							|
| shop			| 			|  <p>Contacts's shop.</p>							|
| saddress			| 			|  <p>Contacts's saddress.</p>							|
| baddress			| 			|  <p>Contacts's baddress.</p>							|
| phone			| 			|  <p>Contacts's phone.</p>							|
| mobile			| 			|  <p>Contacts's mobile.</p>							|
| type			| 			|  <p>Contacts's type.</p>							|
| contact			| 			|  <p>Contacts's contact.</p>							|
| company			| 			|  <p>Contacts's company.</p>							|

## Delete contacts



	DELETE /contacts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve contacts



	GET /contacts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Update contacts



	PUT /contacts/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Contacts's name.</p>							|
| email			| 			|  <p>Contacts's email.</p>							|
| shop			| 			|  <p>Contacts's shop.</p>							|
| saddress			| 			|  <p>Contacts's saddress.</p>							|
| baddress			| 			|  <p>Contacts's baddress.</p>							|
| phone			| 			|  <p>Contacts's phone.</p>							|
| mobile			| 			|  <p>Contacts's mobile.</p>							|
| type			| 			|  <p>Contacts's type.</p>							|
| contact			| 			|  <p>Contacts's contact.</p>							|
| company			| 			|  <p>Contacts's company.</p>							|

# Invoice

## Create invoice



	POST /invoices


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| invoice_number			| 			|  <p>Invoice's invoice_number.</p>							|
| invoice_date			| 			|  <p>Invoice's invoice_date.</p>							|
| invoice_due			| 			|  <p>Invoice's invoice_due.</p>							|
| client			| 			|  <p>Invoice's client.</p>							|
| status			| 			|  <p>Invoice's status.</p>							|
| line_items			| 			|  <p>Invoice's line_items.</p>							|
| subtotal			| 			|  <p>Invoice's subtotal.</p>							|
| total			| 			|  <p>Invoice's total.</p>							|
| discount			| 			|  <p>Invoice's discount.</p>							|
| tax			| 			|  <p>Invoice's tax.</p>							|
| notes			| 			|  <p>Invoice's notes.</p>							|
| shipping_charge			| 			|  <p>Invoice's shipping_charge.</p>							|
| advance_payment			| 			|  <p>Invoice's advance_payment.</p>							|
| desc			| 			|  <p>Invoice's desc.</p>							|

## Delete invoice



	DELETE /invoices/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve invoice



	GET /invoices/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve invoices



	GET /invoices


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve invoices/po/so



	GETSALE /invoices


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update invoice



	PUT /invoices/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| invoice_number			| 			|  <p>Invoice's invoice_number.</p>							|
| invoice_date			| 			|  <p>Invoice's invoice_date.</p>							|
| invoice_due			| 			|  <p>Invoice's invoice_due.</p>							|
| client			| 			|  <p>Invoice's client.</p>							|
| status			| 			|  <p>Invoice's status.</p>							|
| line_items			| 			|  <p>Invoice's line_items.</p>							|
| subtotal			| 			|  <p>Invoice's subtotal.</p>							|
| total			| 			|  <p>Invoice's total.</p>							|
| discount			| 			|  <p>Invoice's discount.</p>							|
| tax			| 			|  <p>Invoice's tax.</p>							|
| notes			| 			|  <p>Invoice's notes.</p>							|
| shipping_charge			| 			|  <p>Invoice's shipping_charge.</p>							|
| advance_payment			| 			|  <p>Invoice's advance_payment.</p>							|
| desc			| 			|  <p>Invoice's desc.</p>							|

# Items

## Create items



	POST /items


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| name			| 			|  <p>Items's name.</p>							|
| sku			| 			|  <p>Items's sku.</p>							|
| desc			| 			|  <p>Items's desc.</p>							|
| quantity			| 			|  <p>Items's quantity.</p>							|
| unit			| 			|  <p>Items's unit.</p>							|
| unitprice			| 			|  <p>Items's unitprice.</p>							|
| sellingprice			| 			|  <p>Items's sellingprice.</p>							|
| category			| 			|  <p>Items's category.</p>							|
| expiredate			| 			|  <p>Items's expiredate.</p>							|
| tax			| 			|  <p>Items's tax.</p>							|
| vendor			| 			|  <p>Items's vendor.</p>							|
| status			| 			|  <p>Items's status.</p>							|
| isservice			| 			|  <p>Items's isservice.</p>							|
| shop			| 			|  <p>Items's shop.</p>							|
| createdby			| 			|  <p>Items's createdby.</p>							|

## Delete items



	DELETE /items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve items



	GET /items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Update items



	PUT /items/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| name			| 			|  <p>Items's name.</p>							|
| sku			| 			|  <p>Items's sku.</p>							|
| desc			| 			|  <p>Items's desc.</p>							|
| quantity			| 			|  <p>Items's quantity.</p>							|
| unit			| 			|  <p>Items's unit.</p>							|
| unitprice			| 			|  <p>Items's unitprice.</p>							|
| sellingprice			| 			|  <p>Items's sellingprice.</p>							|
| category			| 			|  <p>Items's category.</p>							|
| expiredate			| 			|  <p>Items's expiredate.</p>							|
| tax			| 			|  <p>Items's tax.</p>							|
| vendor			| 			|  <p>Items's vendor.</p>							|
| status			| 			|  <p>Items's status.</p>							|
| isservice			| 			|  <p>Items's isservice.</p>							|
| shop			| 			|  <p>Items's shop.</p>							|
| createdby			| 			|  <p>Items's createdby.</p>							|

# PasswordReset

## Send email



	POST /password-resets


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email address to receive the password reset token.</p>							|
| link			| String			|  <p>Link to redirect user.</p>							|

## Submit password



	PUT /password-resets/:token


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Verify token



	GET /password-resets/:token


# Shop

## Create shop



	POST /shops


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| name			| 			|  <p>Shop's name.</p>							|
| address			| 			|  <p>[place Shop's address[place.</p>							|
| pin			| 			|  <p>Shop's pin].</p>							|

## Delete shop



	DELETE /shops/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

## Retrieve shop



	GET /shops/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve shops



	GET /shops


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update shop



	PUT /shops/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|
| name			| 			|  <p>Shop's name.</p>							|
| address			| 			|  <p>[place Shop's address[place.</p>							|
| pin			| 			|  <p>Shop's pin].</p>							|

## Delete shop



	UPLOAD /shops/logo/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>master access token.</p>							|

# Tax

## Create tax



	POST /taxes


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Tax's name.</p>							|
| code			| 			|  <p>Tax's code.</p>							|
| rate			| 			|  <p>Tax's rate.</p>							|
| type			| 			|  <p>Tax's type.</p>							|
| shop			| 			|  <p>Tax's shop.</p>							|

## Delete tax



	DELETE /taxes/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve tax



	GET /taxes/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve taxes



	GET /taxes


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update tax



	PUT /taxes/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| name			| 			|  <p>Tax's name.</p>							|
| code			| 			|  <p>Tax's code.</p>							|
| rate			| 			|  <p>Tax's rate.</p>							|
| type			| 			|  <p>Tax's type.</p>							|
| shop			| 			|  <p>Tax's shop.</p>							|

# Transaction

## Create transaction



	POST /transactions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| txnType			| 			|  <p>Transaction's txnType.</p>							|
| amount			| 			|  <p>Transaction's amount.</p>							|
| account			| 			|  <p>Transaction's account.</p>							|
| lineAccount			| 			|  <p>Transaction's lineAccount.</p>							|
| customer			| 			|  <p>Transaction's customer.</p>							|
| supplier			| 			|  <p>Transaction's supplier.</p>							|
| tax			| 			|  <p>Transaction's tax.</p>							|
| paymentMethod			| 			|  <p>Transaction's paymentMethod.</p>							|
| currencyType			| 			|  <p>Transaction's currencyType.</p>							|
| invoice			| 			|  <p>Transaction's invoice.</p>							|
| bill			| 			|  <p>Transaction's bill.</p>							|
| shop			| 			|  <p>Transaction's shop.</p>							|

## Delete transaction



	DELETE /transactions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve transaction



	GET /transactions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve transactions



	GET /transactions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update transaction



	PUT /transactions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| txnType			| 			|  <p>Transaction's txnType.</p>							|
| amount			| 			|  <p>Transaction's amount.</p>							|
| account			| 			|  <p>Transaction's account.</p>							|
| lineAccount			| 			|  <p>Transaction's lineAccount.</p>							|
| customer			| 			|  <p>Transaction's customer.</p>							|
| supplier			| 			|  <p>Transaction's supplier.</p>							|
| tax			| 			|  <p>Transaction's tax.</p>							|
| paymentMethod			| 			|  <p>Transaction's paymentMethod.</p>							|
| currencyType			| 			|  <p>Transaction's currencyType.</p>							|
| invoice			| 			|  <p>Transaction's invoice.</p>							|
| bill			| 			|  <p>Transaction's bill.</p>							|
| shop			| 			|  <p>Transaction's shop.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


