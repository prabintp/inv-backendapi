import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Blog, { schema } from './model'

const router = new Router()
const { name, content } = schema.tree

/**
 * @api {post} /blogs Create blog
 * @apiName CreateBlog
 * @apiGroup Blog
 * @apiParam name Blog's name.
 * @apiParam content Blog's content.
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 */
router.post('/',
  body({ name, content }),
  create)

/**
 * @api {get} /blogs Retrieve blogs
 * @apiName RetrieveBlogs
 * @apiGroup Blog
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of blogs.
 * @apiSuccess {Object[]} rows List of blogs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /blogs/:id Retrieve blog
 * @apiName RetrieveBlog
 * @apiGroup Blog
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /blogs/:id Update blog
 * @apiName UpdateBlog
 * @apiGroup Blog
 * @apiParam name Blog's name.
 * @apiParam content Blog's content.
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 */
router.put('/:id',
  body({ name, content }),
  update)

/**
 * @api {delete} /blogs/:id Delete blog
 * @apiName DeleteBlog
 * @apiGroup Blog
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Blog not found.
 */
router.delete('/:id',
  destroy)

export default router
