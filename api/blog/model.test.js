import { Blog } from '.'

let blog

beforeEach(async () => {
  blog = await Blog.create({ name: 'test', content: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = blog.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(blog.id)
    expect(view.name).toBe(blog.name)
    expect(view.content).toBe(blog.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = blog.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(blog.id)
    expect(view.name).toBe(blog.name)
    expect(view.content).toBe(blog.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
